package de.wwweasel.Gateway.controller;

import de.wwweasel.Gateway.config.BlaConfig;
import de.wwweasel.Gateway.config.OidcUserDTO;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.autoconfigure.RefreshAutoConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ReactiveClientRegistrationRepository;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.oidc.StandardClaimNames;
import org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.util.List;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.mockOidcLogin;


//@WebFluxTest(controllers = {Controller.class})//, properties = {"spring.cloud.vault.enabled=false", "spring.profiles.active=production", "security.config=production"}
//@Import({RefreshAutoConfiguration.class, BlaConfig.class})
//@ActiveProfiles("test")
class ControllerTest {

//    @Autowired
//    WebTestClient webTestClient;
//
//    @MockBean
//    ReactiveClientRegistrationRepository reactiveClientRegistrationRepository;
//
//    @Test
//    @DisplayName("Should retrun 200 even without Authentication")
//    void getUser_noAuthentication() throws Exception{
//
//        webTestClient
//            .get()
//            .uri("/api/user")
//            .exchange()
//            .expectStatus().isOk();
//
//    }
//
//
//    @Test
//    @DisplayName("Should work with Authentication")
//    void chat_noAuthentication(){
//
//        webTestClient
//                .get()
//                .uri("/api/chatfallback")
//                .exchange()
//                .expectStatus().isUnauthorized();
//    }
//
//    @Test
//    @DisplayName("Should not work without Authentication")
//    void chat_withAuthentication(){
//        OidcUserDTO oidcUserDTO = new OidcUserDTO("ni", "ni", "haber", List.of("PERSON"), "ni@ni.com", "a047d62a-078b-46cf-9855-59f062787501");
//
//        webTestClient.mutateWith(configureMockOidcLogin(oidcUserDTO))
//                .get()
//                .uri("/api/chatfallback")
//                .exchange()
//                .expectStatus().isOk();
//    }
//
//    private SecurityMockServerConfigurers.OidcLoginMutator configureMockOidcLogin(OidcUserDTO expectedUser) {
//        return mockOidcLogin().idToken(builder -> {
//            builder.claim(StandardClaimNames.PREFERRED_USERNAME, expectedUser.getUsername());
//            builder.claim(StandardClaimNames.GIVEN_NAME, expectedUser.getFirstName());
//            builder.claim(StandardClaimNames.FAMILY_NAME, expectedUser.getLastName());
//        });
//    }
//
//
//    @Test
//    @DisplayName("Should return 403 WITHOUT Authentication WITHOUT CSRF")
//    void logout_noAuthentication_oCSRF() {
//
//        webTestClient
//                .post().uri("/logout")
//                .exchange()
//                .expectStatus().isForbidden();
//    }
//
//    @Test
//    @DisplayName("Should return 403 WITH Authentication WITHOUT CSRF")
//    void logout_withAuthentication_noCSRF() {
//
//        webTestClient
//                .mutateWith(mockOidcLogin())
//                .post().uri("/logout")
//                .exchange()
//                .expectStatus().isForbidden();
//    }
//
//    @Test
//    @DisplayName("Should redirect with 302 with Authentication and CSRF")
//    void logout_withAuthentication_withCSRF() {
//
//        Mockito.when(this.reactiveClientRegistrationRepository.findByRegistrationId("keycloak")).thenReturn(Mono.just(testClientRegistration()));
//
//        webTestClient
//                .mutateWith(mockOidcLogin())
//                .mutateWith(csrf())
//                .post().uri("/logout")
//                .exchange()
//                .expectStatus().isFound();
//    }
//
//    private ClientRegistration testClientRegistration() {
//        return ClientRegistration
//                .withRegistrationId("keycloak")
//                .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
//                .clientId("wwweasel")
//                .authorizationUri("https://auth.haberpursch.dev/auth")
//                .tokenUri("https://auth.haberpursch.dev/token")
//                .redirectUri("https://haberpursch.dev")
//                .build();
//    }

}
package de.wwweasel.Gateway.config;

import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;

// This motherfucker will be called in the SecurityConfig form the successHandler after a successful login!
public class RabbitProducer {

    private final RabbitTemplate rabbitTemplate;

    private final TopicExchange topicExchange;

    public RabbitProducer(RabbitTemplate rabbitTemplate, TopicExchange topicExchange) {
        this.rabbitTemplate = rabbitTemplate;
        this.topicExchange = topicExchange;
    }

    public void loginSuccess(Authentication authentication){

        if(authentication.getPrincipal() instanceof OidcUser){
            OidcUser oidcUser = (OidcUser) authentication.getPrincipal();
            String routingKey = "login.success";

            OidcUserDTO oidcUserDTO = new OidcUserDTO(
                    oidcUser.getPreferredUsername(),
                    oidcUser.getGivenName(),
                    oidcUser.getFamilyName(),
                    oidcUser.getClaimAsStringList("roles"),
                    oidcUser.getEmail(),
                    oidcUser.getSubject()
            );
            //PersonDTO dto = new PersonDTO(oidcUser.getFullName(), oidcUser.getEmail(), oidcUser.getGivenName());
            rabbitTemplate.convertAndSend(topicExchange.getName(), routingKey, oidcUserDTO);
        }else{
            System.out.println("This should throw an Error!");
            return;
        }
    }

}

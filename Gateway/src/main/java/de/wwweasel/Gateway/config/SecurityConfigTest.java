package de.wwweasel.Gateway.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.oauth2.client.oidc.web.server.logout.OidcClientInitiatedServerLogoutSuccessHandler;
import org.springframework.security.oauth2.client.registration.ReactiveClientRegistrationRepository;
import org.springframework.security.oauth2.client.web.server.ServerOAuth2AuthorizedClientRepository;
import org.springframework.security.oauth2.client.web.server.WebSessionServerOAuth2AuthorizedClientRepository;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.security.web.server.authentication.HttpStatusServerEntryPoint;
import org.springframework.security.web.server.authentication.logout.ServerLogoutSuccessHandler;
import org.springframework.security.web.server.csrf.CookieServerCsrfTokenRepository;
import org.springframework.security.web.server.csrf.CsrfToken;
import org.springframework.security.web.server.header.ReferrerPolicyServerHttpHeadersWriter;
import org.springframework.web.reactive.config.CorsRegistry;
import org.springframework.web.reactive.config.WebFluxConfigurer;
import org.springframework.web.server.WebFilter;
import reactor.core.publisher.Mono;


//@Configuration
//@EnableWebFluxSecurity
//@ConditionalOnProperty( value="security.config", havingValue = "test", matchIfMissing = true)
public class SecurityConfigTest implements WebFluxConfigurer {


    private String contentSecurityPolicy =
            "default-src 'self' https://gateway:8081; " +
                    "frame-src 'self' data:; " +
                    "script-src 'self'; " +
                    "style-src 'self'; " +
                    "img-src 'self' data:; " +
                    "font-src 'self' data:; " +
                    "form-action 'self'; " +
                    "media-src 'self'; " +
                    "object-src 'none'; "
            ;


    @Bean
    public SecurityWebFilterChain securityWebFilterChain(ServerHttpSecurity http, ReactiveClientRegistrationRepository reactiveClientRegistrationRepository){
        http
                .authorizeExchange()
                .pathMatchers("/", "/*.css", "/*.js", "/favicon.ico","/css/**,","/icons/**","/fonts/**","/img/**","/js/**","/css/*.css").permitAll()
                .pathMatchers("/actuator/**").permitAll()
                .pathMatchers("/api/user").permitAll()
                .pathMatchers("/auth/**").permitAll()
                //.pathMatchers("/eureka/**").permitAll()
                .anyExchange().authenticated()
                // hasRole() is only possible if this Gateway also is a ResourceServer
                .and()
                // The following returns a 401 UNAUTHORIZED for AjaxRequests, instead of redirecting to Keycloak.
                .exceptionHandling().authenticationEntryPoint( new HttpStatusServerEntryPoint(HttpStatus.UNAUTHORIZED) )
                .and()
                .oauth2Login()
                .and()
                .csrf()
                .csrfTokenRepository(CookieServerCsrfTokenRepository.withHttpOnlyFalse())
                .and()
                .cors()
                .and()
                .headers()
                .contentSecurityPolicy(this.contentSecurityPolicy)// XSS-Attack-Protection
                .and()
                .referrerPolicy(ReferrerPolicyServerHttpHeadersWriter.ReferrerPolicy.STRICT_ORIGIN_WHEN_CROSS_ORIGIN)
                .and()
                .frameOptions().disable()
                .permissionsPolicy().policy("camera=(), fullscreen=(self), geolocation=(), gyroscope=(), magnetometer=(), microphone=(), midi=(), payment=(), sync-xhr=()")
        ;
//            .and()
        //                .redirectToHttps(redirect -> redirect.httpsRedirectWhen(e -> e.getRequest().getHeaders().containsKey("X-Forwarded-Proto")));

        return http.build();
    }


    @Bean
    WebFilter csrfWebFilter() {
        // Required because of https://github.com/spring-projects/spring-security/issues/5766
        return (exchange, chain) -> {
            exchange.getResponse().beforeCommit(() -> Mono.defer(() -> {
                Mono<CsrfToken> csrfToken = exchange.getAttribute(CsrfToken.class.getName());
                return csrfToken != null ? csrfToken.then() : Mono.empty();
            }));
            return chain.filter(exchange);
        };
    }

    // Store the AccessToken( JWT ) also in the WebSession on Redis, just like the IDToken( JWT )
    @Bean
    ServerOAuth2AuthorizedClientRepository serverOAuth2AuthorizedClientRepository(){
        return new WebSessionServerOAuth2AuthorizedClientRepository();
    }

    @Value("${cors_url}")
    private String cors_url;
    @Value("${cors_methods}")
    private String[] cors_methods;
    /*
        Implement CORS here, because in the application.yml
        it does not work.
     */
    @Override
    public void addCorsMappings(CorsRegistry corsRegistry) {
        corsRegistry.addMapping("/**")
                .allowedOrigins(cors_url)
                .allowedMethods(cors_methods)
        ;
        WebFluxConfigurer.super.addCorsMappings(corsRegistry);
    }

}

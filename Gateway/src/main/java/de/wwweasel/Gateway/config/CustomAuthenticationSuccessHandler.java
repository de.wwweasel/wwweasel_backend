package de.wwweasel.Gateway.config;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.server.DefaultServerRedirectStrategy;
import org.springframework.security.web.server.ServerRedirectStrategy;
import org.springframework.security.web.server.WebFilterExchange;
import org.springframework.security.web.server.authentication.ServerAuthenticationSuccessHandler;
import org.springframework.security.web.server.savedrequest.ServerRequestCache;
import org.springframework.security.web.server.savedrequest.WebSessionServerRequestCache;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;


import java.net.URI;


@Component
public class CustomAuthenticationSuccessHandler implements ServerAuthenticationSuccessHandler {

    private final RabbitProducer rabbitProducer;
    private URI location = URI.create("/"); // Redirect to root / which means the SPA can take over and redirect to /user
    private ServerRedirectStrategy redirectStrategy = new DefaultServerRedirectStrategy();
    private ServerRequestCache requestCache = new WebSessionServerRequestCache();

    public CustomAuthenticationSuccessHandler(RabbitProducer rabbitProducer) {
        this.rabbitProducer = rabbitProducer;
    }

    public void setRequestCache(ServerRequestCache requestCache) {
        Assert.notNull(requestCache, "requestCache cannot be null");
        this.requestCache = requestCache;
    }

//    public Mono<Void> onAuthenticationSuccess(WebFilterExchange webFilterExchange, Authentication authentication) {
//        System.out.println(" !!! CustomAuthenticationSuccessHandler triggered !!! ");
//        this.rabbitProducer.loginSuccess( authentication );
//        ServerWebExchange exchange = webFilterExchange.getExchange();
//        return this.requestCache.getRedirectUri(exchange).defaultIfEmpty(this.location).flatMap((location) -> {
//            return this.redirectStrategy.sendRedirect(exchange, location);
//        });
//    }

    @Override
    public Mono<Void> onAuthenticationSuccess(WebFilterExchange webFilterExchange, Authentication authentication) {
        System.out.println(" !!! CustomAuthenticationSuccessHandler triggered AGAIN!!! ");
        this.rabbitProducer.loginSuccess( authentication );

        // The following set the LOCATION to the one the user wanted to reach originally
        // When the original Location is emtpy it will be set to '/test/userinfo'
        ServerWebExchange exchange = webFilterExchange.getExchange();
        return this.requestCache.getRedirectUri(exchange).defaultIfEmpty(this.location).flatMap(location -> {
            //System.out.println("LOCATION: " + location);
            if (location.toASCIIString().isBlank()) {
                location = URI.create("/");
            }
            return this.redirectStrategy.sendRedirect(exchange, location);
        });
    }

    public void setLocation(URI location) {
        Assert.notNull(location, "location cannot be null");
        this.location = location;
    }

    public void setRedirectStrategy(ServerRedirectStrategy redirectStrategy) {
        Assert.notNull(redirectStrategy, "redirectStrategy cannot be null");
        this.redirectStrategy = redirectStrategy;
    }
}

package de.wwweasel.Gateway.config;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    @Bean
    public TopicExchange exchange() {
        return new TopicExchange("customExchange");
    }

//    @Bean
//    public Queue loginQueue() {
//        return new Queue("loginQueue");
//    }
//
//    @Bean
//    public Binding loginBinding(@Qualifier("loginQueue") Queue queue, TopicExchange topicExchange) {
//        return BindingBuilder
//                .bind(queue)
//                .to(topicExchange)
//                .with("login.*")
//                ;
//    }

    @Bean
    public MessageConverter messageConverter(){
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    AmqpTemplate amqpTemplate(ConnectionFactory connectionFactory){
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter( messageConverter() );
        return rabbitTemplate;
    }

    @Bean
    public RabbitProducer rabbitProducer(RabbitTemplate rabbitTemplate, TopicExchange topicExchange) {
        return new RabbitProducer(rabbitTemplate, topicExchange);
    }

//    @Bean
//    RabbitConsumer rabbitConsumer(){
//        return new RabbitConsumer();
//    }

}

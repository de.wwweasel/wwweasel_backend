package de.wwweasel.Gateway.config;

import org.springframework.amqp.rabbit.annotation.RabbitListener;

public class RabbitConsumer {

    @RabbitListener(queues="loginQueue")
    public void receive(PersonDTO user) {
        System.out.println("RabbitMessage: " + user );
    }

}

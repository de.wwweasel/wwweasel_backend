package de.wwweasel.Gateway.controller;

import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

@Component
public class SpaWebFilter implements WebFilter {
    /**
     * Forwards any unmapped paths (except those containing a period) to the client {@code index.html}.
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
        String path = exchange.getRequest().getURI().getPath();
        if (
            !path.startsWith("/api/user") &&
            !path.startsWith("/api/chat/chatsByEmail") &&
            !path.startsWith("/api/chat/message") &&
            !path.startsWith("/api/chatfallback") &&
            !path.startsWith("/api/imgfallback") &&
            !path.startsWith("/api/img/**") &&
            !path.startsWith("/management") &&
            !path.startsWith("/auth") &&
            //!path.startsWith("/eureka") &&
            //!path.startsWith("/services") &&
            //!path.startsWith("/swagger") &&
            //!path.startsWith("/v2/api-docs") &&
            //!path.startsWith("/v3/api-docs") &&
            //!path.startsWith("/img") &&
            //!path.startsWith("/img/test") &&
            //!path.startsWith("/chat") &&
            !path.startsWith("/actuator") &&
            !path.startsWith("/login") && // Mandatory or the redirect from keycloak to gateway at auth dance wont work
            !path.startsWith("/logout") &&
//                !path.startsWith("/css") &&
//                !path.startsWith("/js") &&
//                !path.startsWith("/fonts") &&
//                !path.startsWith("/icons") &&
//                !path.startsWith("/img") &&
            path.matches("[^\\\\.]*")
        ) {
            return chain.filter(exchange.mutate().request(exchange.getRequest().mutate().path("/index.html").build()).build());
        }
        return chain.filter(exchange);
    }

}

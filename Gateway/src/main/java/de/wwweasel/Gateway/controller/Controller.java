package de.wwweasel.Gateway.controller;

import de.wwweasel.Gateway.config.BlaConfig;
import de.wwweasel.Gateway.config.OidcUserDTO;
import de.wwweasel.Gateway.dto.ChatroomWebDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.context.scope.refresh.RefreshScopeRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import java.util.HashSet;

@RestController
@RefreshScope
public class Controller {


    @Autowired
    BlaConfig blaConfig;

    @EventListener({RefreshScopeRefreshedEvent.class, ApplicationReadyEvent.class})
    public void onEvent(){
        System.out.println("onEvent: " + blaConfig.getTest());
    }

    @GetMapping("/api/user")
    public Mono<OidcUserDTO> getUser(@AuthenticationPrincipal OidcUser oidcUser) {
        if(oidcUser!=null){
            OidcUserDTO dto = new OidcUserDTO(
                    oidcUser.getPreferredUsername(),
                    oidcUser.getGivenName(),
                    oidcUser.getFamilyName(),
                    oidcUser.getClaimAsStringList("roles"),
                    oidcUser.getEmail(),
                    oidcUser.getSubject()
            );
            return Mono.just(dto);
        }
        return null;
    }

    @GetMapping("/api/chatfallback")
    public HashSet<ChatroomWebDTO> chatfallback(){
        return new HashSet<ChatroomWebDTO>();
    }

    @GetMapping("/api/imgfallback")
    public String imgfallback(){
        return "imgfallback";
    }

    @GetMapping("/api/keycloakfallback")
    public String keycloakfallback(){
        return "keycloakfallback";
    }


}

package de.wwweasel.Gateway.exceptions;

import java.util.ArrayList;
import java.util.List;

public class ExceptionDTO {

    private final int status;
    private final String message;
    private List<ValidationError> errors = new ArrayList<>();

    public ExceptionDTO(int status, String message) {
        this.status = status;
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public void addValidationError(String field, String message){
        this.errors.add( new ValidationError(field, message) );
    }

    //
    private static class ValidationError {
        private final String field;
        private final String message;

        public ValidationError(String field, String message) {
            this.field = field;
            this.message = message;
        }

        public String getField() {
            return field;
        }

        public String getMessage() {
            return message;
        }
    }

}

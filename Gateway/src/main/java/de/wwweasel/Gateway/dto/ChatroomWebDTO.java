package de.wwweasel.Gateway.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ChatroomWebDTO {

    private Long id;
    private String name;
    private List<String> persons = new ArrayList<>();


    public ChatroomWebDTO(){ }

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }
    public String getName() { return name; }
    public void setName(String name) { this.name = name; }
    public List<String> getPersons() { return persons; }
    public void setPersons(List<String> persons) { this.persons = persons; }

    @Override
    public String toString() {
        return "ChatroomDTO{" +
                "chatroom_id=" + id +
                ", chatroom_name='" + name + '\'' +
                ", persons=" + persons +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChatroomWebDTO that = (ChatroomWebDTO) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

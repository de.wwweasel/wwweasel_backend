version: '3.8'

networks:
  swarm:
    external: true


services:

  traefik:
    image: "traefik:v2.6.1"
    #user: guest # Not really possible because of docker.sock
#    secrets:
#      - source: traefik_public_key
#        target: /etc/traefik/certs/traefik.pem
#      - source: traefik_private_key
#        target: /etc/traefik/certs/traefik-key.pem
    configs:
      - source: traefik_config
        target: /etc/traefik/traefik.yml
      - source: traefik_dynamic_config
        target: /etc/traefik/dynamic_config_files/dynamic.yml
    ports:
      - "80:80"
      - "443:443"
      # (Optional) Expose Dashboard
      #- "8080:8080"  # Don't do this in production!
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro
      - ./CONFIG/Letsencypt:/letsencrypt # SSL Certs from Letsencrypt
    networks:
      - swarm
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=wwweasel"
    deploy:
      replicas: 1
      resources:
        limits:
          cpus: '0.5'
          memory: 1024M
        reservations:
          cpus: '0.25'
          memory: 512M


  keycloakpostgres:
    image: postgres:13-alpine
    user: postgres
    environment:
      POSTGRES_DB: postgres
      POSTGRES_USER_FILE: /run/secrets/keycloakpostgres_user
      POSTGRES_PASSWORD_FILE: /run/secrets/keycloakpostgres_password
    secrets:
      - keycloakpostgres_user
      - keycloakpostgres_password
      - source: keycloakpostgres_public_key
        target: /var/lib/postgresql/server.crt
        uid: '70'
        gid: '70'
        mode: 0400
      - source: keycloakpostgres_private_key
        target: /var/lib/postgresql/server.key
        uid: '70'
        gid: '70'
        mode: 0400
      - source: keycloakpostgres_intermediateCA
        target: /var/lib/postgresql/root.crt
        uid: '70'
        gid: '70'
        mode: 0400
    command: [ "-c", "ssl=on", "-c", "ssl_cert_file=/var/lib/postgresql/server.crt", "-c", "ssl_key_file=/var/lib/postgresql/server.key" ]
    volumes:
      - ./DB_bindmounts/keycloak_postgres_bindmount:/var/lib/postgresql/data
    networks:
      - swarm
    deploy:
      replicas: 1
      resources:
        limits:
          cpus: '0.25'
          memory: 1024M
        reservations:
          cpus: '0.125'
          memory: 512M


  keycloak:
    image: jboss/keycloak:16.1.0
    environment:
      KEYCLOAK_HTTP_PORT: 8080
      KEYCLOAK_HTTPS_PORT: 8443
      KEYCLOAK_USER_FILE: /run/secrets/keycloak_user
      KEYCLOAK_PASSWORD_FILE: /run/secrets/keycloak_password
      DB_VENDOR: POSTGRES
      DB_ADDR: keycloakpostgres
      DB_DATABASE: postgres
      DB_USER_FILE: /run/secrets/keycloakpostgres_user
      DB_PASSWORD_FILE: /run/secrets/keycloakpostgres_password
      DB_SCHEMA: public
      PROXY_ADDRESS_FORWARDING: 'true'
      KEYCLOAK_IMPORT: /opt/jboss/keycloak/imports/realm-production.json
    secrets:
      - keycloak_user
      - keycloak_password
      - keycloakpostgres_user
      - keycloakpostgres_password
      - source: keycloak_public_key
        target: /etc/x509/https/tls.crt
        uid: '1000'
        gid: '0'
        mode: 0400
      - source: keycloak_private_key
        target: /etc/x509/https/tls.key
        uid: '1000'
        gid: '0'
        mode: 0400
    configs:
      - source: keycloak_config
        target: /opt/jboss/keycloak/imports/realm-production.json
        uid: '1000' # jboss
        gid: '0' # jboss
        mode: 0440
    depends_on:
      - keycloakpostgres
    networks:
      - swarm
      #- keycloakpostgres
    volumes:
      - ./Keycloak/themes/wwweasel:/opt/jboss/keycloak/themes/wwweasel
    labels:
      - traefik.enable=true
      - traefik.docker.network=wwweasel
      - traefik.http.routers.keycloak-secure.entrypoints=web,websecure # web=http websecure=https
      - traefik.http.routers.keycloak-secure.tls=true
      - traefik.http.routers.keycloak-secure.service=keycloak
      - traefik.http.routers.keycloak-secure.rule=Host(`auth.haberpursch.dev`)
      - traefik.http.services.keycloak.loadbalancer.server.scheme=https
      - traefik.http.services.keycloak.loadbalancer.server.port=8443
      - traefik.http.routers.keycloak-secure.tls.certresolver=production
    deploy:
      replicas: 1
      resources:
        limits:
          cpus: '0.5'
          memory: 1024M
        reservations:
          cpus: '0.25'
          memory: 1024M


  rabbitmq:
    image: rabbitmq:3.9-management-alpine
    user: rabbitmq
    secrets:
      - source: rabbitmq_public_key
        target: /run/secrets/rabbitmq_public_key
        uid: '100' #rabbitmq
        gid: '101'
        mode: 400 # Cannot be writable!
      - source: rabbitmq_private_key
        target: /run/secrets/rabbitmq_private_key
        uid: '100'
        gid: '101'
        mode: 400 # Cannot be writable!
      - source: rabbitmq_intermediateCA
        target: /run/secrets/rabbitmq_intermediateCA
        uid: '100'
        gid: '101'
        mode: 400 # Cannot be writable!
      - source: rabbitmq_user
        target: /run/secrets/rabbitmq_user
      - source: rabbitmq_password
        target: /run/secrets/rabbitmq_password
    configs:
      - source: rabbitmq_config
        target: /etc/rabbitmq/rabbitmq.conf
        uid: '100'
        gid: '101'
        mode: 0440 # Cannot be writable!
      - source: rabbitmq_definitions
        target: /rabbitmq_definitions
        uid: '100'
        gid: '101'
        mode: 400 # Cannot be writable!
    networks:
      - swarm
    deploy:
      replicas: 1
      resources:
        limits:
          cpus: '0.5'
          memory: 1024M
        reservations:
          cpus: '0.25'
          memory: 1024M


  config:
    image: registry.gitlab.com/de.wwweasel/wwweasel_backend/config:1.0
    volumes:
      - ./CONFIG/wwweasel_config/:/home/config/repo
    secrets:
      - config_basicauth_username
      - config_basicauth_password
      - config_keystore
      - config_keystore_password
      - configEncrypt_keystore
      - configEncrypt_keystore_password
      - rabbitmq_user
      - rabbitmq_password
      - config_gitlab_username
      - config_gitlab_accesstoken
    environment:
      PROFILE: production
      CONFIGSERVER_KEYSTORE: /run/secrets/config_keystore
      CONFIGSERVER_ENCRYPT_KEYSTORE: /run/secrets/configEncrypt_keystore
    depends_on:
      - rabbitmq
    networks:
      - swarm
    deploy:
      replicas: 1
      resources:
        limits:
          cpus: '0.25'
          memory: 1024M
        reservations:
          cpus: '0.125'
          memory: 512M


  redis:
    image: redis:6.2.6-alpine
    user: redis
    secrets:
      - source: redis_public_key
        target: /run/secrets/redis_public_key
        uid: '999'
        gid: '1000'
        mode: 400 # Cannot be writable!
      - source: redis_private_key
        target: /run/secrets/redis_private_key
        uid: '999'
        gid: '1000'
        mode: 400 # Cannot be writable!
      - source: redis_intermediateCA
        target: /run/secrets/redis_intermediateCA
        uid: '999'
        gid: '1000'
        mode: 400 # Cannot be writable!
    configs:
      - source: redis_config
        target: /redis_config
        uid: '999'
        gid: '1000'
        mode: 0440 # Cannot be writable!
    command: [ "redis-server", "/redis_config" ]
    networks:
      - swarm
    deploy:
      replicas: 1
      resources:
        limits:
          cpus: '0.5'
          memory: 1024M
        reservations:
          cpus: '0.25'
          memory: 512M


  gateway:
    image: registry.gitlab.com/de.wwweasel/wwweasel_backend/gateway:1.0
    secrets:
      - config_basicauth_username
      - config_basicauth_password
      - gateway_keystore
      - gateway_keystore_password
      - gateway_roleId
      - gateway_secretId
    environment:
      PROFILE: production
      GATEWAY_KEYSTORE: /run/secrets/gateway_keystore
    depends_on:
      - keycloak
    networks:
      - swarm
    labels:
      - traefik.enable=true
      - traefik.docker.network=wwweasel
      - traefik.http.routers.gateway-secure.entrypoints=web,websecure # web=http websecure=https
      - traefik.http.routers.gateway-secure.tls=true
      - traefik.http.routers.gateway-secure.service=gateway
      - traefik.http.routers.gateway-secure.rule=Host(`haberpursch.dev`)
      - traefik.http.services.gateway.loadbalancer.server.scheme=https
      - traefik.http.services.gateway.loadbalancer.server.port=8081
      - traefik.http.routers.gateway-secure.tls.certresolver=production
    extra_hosts:
      - "auth.haberpursch.dev:host-gateway"
      #- "host.docker.internal:host-gateway"
    deploy:
      replicas: 1
      resources:
        limits:
          cpus: '2.0'
          memory: 4096M
        reservations:
          cpus: '1.0'
          memory: 4096M


  chatpostgres:
    image: postgres:13-alpine
    user: postgres
    environment:
      POSTGRES_DB: postgres
      POSTGRES_USER_FILE: /run/secrets/chatpostgres_user
      POSTGRES_PASSWORD_FILE: /run/secrets/chatpostgres_password
    secrets:
      - chatpostgres_user
      - chatpostgres_password
      - source: chatpostgres_public_key
        target: /var/lib/postgresql/server.crt
        uid: '70'
        gid: '70'
        mode: 0400
      - source: chatpostgres_private_key
        target: /var/lib/postgresql/server.key
        uid: '70'
        gid: '70'
        mode: 0400
      - source: chatpostgres_intermediateCA
        target: /var/lib/postgresql/root.crt
        uid: '70'
        gid: '70'
        mode: 0400
    command: [ "-c", "ssl=on", "-c", "ssl_cert_file=/var/lib/postgresql/server.crt", "-c", "ssl_key_file=/var/lib/postgresql/server.key" ]
    volumes:
      - ./DB_bindmounts/chat_postgres_bindmount:/var/lib/postgresql/data
    networks:
      - swarm
    deploy:
      replicas: 1
      resources:
        limits:
          cpus: '0.25'
          memory: 1024M
        reservations:
          cpus: '0.125'
          memory: 512M


  chatms:
    image: registry.gitlab.com/de.wwweasel/wwweasel_backend/chatms:1.0
    secrets:
      - config_basicauth_username
      - config_basicauth_password
      - chat_keystore
      - chat_keystore_password
      - chat_roleId
      - chat_secretId
    environment:
      PROFILE: production
      CHAT_KEYSTORE: /run/secrets/chat_keystore
    depends_on:
      - rabbitmq
      - chatpostgres
    extra_hosts:
      - "auth.haberpursch.dev:host-gateway"
      #- "host.docker.internal:host-gateway"
    networks:
      - swarm
    deploy:
      replicas: 1
      resources:
        limits:
          cpus: '0.5'
          memory: 1024M
        reservations:
          cpus: '0.25'
          memory: 1024M


  img:
    image: registry.gitlab.com/de.wwweasel/wwweasel_backend/img:1.0
    secrets:
      - config_basicauth_username
      - config_basicauth_password
      - img_keystore
      - img_keystore_password
      - img_roleId
      - img_secretId
    environment:
      PROFILE: production
      IMG_KEYSTORE: /run/secrets/img_keystore
    depends_on:
      - imgpostgres
    networks:
      - swarm
    volumes:
      - ./Images:/home/imgms/Images
    extra_hosts:
      - "auth.haberpursch.dev:host-gateway"
    deploy:
      replicas: 1
      resources:
        limits:
          cpus: '0.5'
          memory: 1024M
        reservations:
          cpus: '0.25'
          memory: 1024M


  imgpostgres:
    image: postgres:13-alpine
    user: postgres
    environment:
      POSTGRES_DB: postgres
      POSTGRES_USER_FILE: /run/secrets/imgpostgres_user
      POSTGRES_PASSWORD_FILE: /run/secrets/imgpostgres_password
    secrets:
      - imgpostgres_user
      - imgpostgres_password
      - source: imgpostgres_public_key
        target: /var/lib/postgresql/server.crt
        uid: '70'
        gid: '70'
        mode: 0400
      - source: imgpostgres_private_key
        target: /var/lib/postgresql/server.key
        uid: '70'
        gid: '70'
        mode: 0400
      - source: imgpostgres_intermediateCA
        target: /var/lib/postgresql/root.crt
        uid: '70'
        gid: '70'
        mode: 0400
    command: [ "-c", "ssl=on", "-c", "ssl_cert_file=/var/lib/postgresql/server.crt", "-c", "ssl_key_file=/var/lib/postgresql/server.key" ]
    volumes:
      - ./DB_bindmounts/img_postgres_bindmount:/var/lib/postgresql/data
    networks:
      - swarm
    deploy:
      replicas: 1
      resources:
        limits:
          cpus: '0.25'
          memory: 1024M
        reservations:
          cpus: '0.125'
          memory: 512M


configs:
  # Traefik
  traefik_config:
    external: true
  traefik_dynamic_config:
    external: true
  # redis
  redis_config:
    external: true
  # rabbitmq
  rabbitmq_config:
    external: true
  rabbitmq_definitions:
    external: true
  # Keycloak
  keycloak_config:
    external: true

secrets:
  # Traefik
  traefik_public_key:
    external: true
  traefik_private_key:
    external: true
  # config
  config_basicauth_username:
    external: true
  config_basicauth_password:
    external: true
  config_keystore:
    external: true
  config_keystore_password:
    external: true
  configEncrypt_keystore:
    external: true
  configEncrypt_keystore_password:
    external: true
  config_gitlab_username:
    external: true
  config_gitlab_accesstoken:
    external: true
  # gateway
  gateway_keystore:
    external: true
  gateway_keystore_password:
    external: true
  gateway_roleId:
    external: true
  gateway_secretId:
    external: true
  # chat
  chat_keystore:
    external: true
  chat_keystore_password:
    external: true
  chat_roleId:
    external: true
  chat_secretId:
    external: true
  # chatPosgres
  chatpostgres_user:
    external: true
  chatpostgres_password:
    external: true
  chatpostgres_public_key:
    external: true
  chatpostgres_private_key:
    external: true
  chatpostgres_intermediateCA:
    external: true
  # img
  img_keystore:
    external: true
  img_keystore_password:
    external: true
  img_roleId:
    external: true
  img_secretId:
    external: true
  # imgPosgres
  imgpostgres_user:
    external: true
  imgpostgres_password:
    external: true
  imgpostgres_public_key:
    external: true
  imgpostgres_private_key:
    external: true
  imgpostgres_intermediateCA:
    external: true
  # Keycloak
  keycloak_user:
    external: true
  keycloak_password:
    external: true
  keycloak_public_key:
    external: true
  keycloak_private_key:
    external: true
  # KeycloakPostgres
  keycloakpostgres_user:
    external: true
  keycloakpostgres_password:
    external: true
  keycloakpostgres_public_key:
    external: true
  keycloakpostgres_private_key:
    external: true
  keycloakpostgres_intermediateCA:
    external: true
  # RabbitMq
  rabbitmq_user:
    external: true
  rabbitmq_password:
    external: true
  rabbitmq_public_key:
    external: true
  rabbitmq_private_key:
    external: true
  rabbitmq_intermediateCA:
    external: true
  # Redis
  redis_public_key:
    external: true
  redis_private_key:
    external: true
  redis_intermediateCA:
    external: true
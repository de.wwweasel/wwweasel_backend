package de.wwweasel.ImgMs.controller;

import de.wwweasel.ImgMs.service.ImageService;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.security.RolesAllowed;

@RestController
public class Controller {

    private ImageService imageService;

    public Controller(ImageService imageService){
        this.imageService = imageService;
    }

    @GetMapping(value = "/api/img/image/{email}", produces = {"image/svg+xml","image/jpeg"})//
    public FileSystemResource imageByEmail(@PathVariable String email, Authentication authentication) throws Exception {
        return imageService.findByEmail(email);
    }

    //@Secured({ "ROLE_WWWEASEL" })
    @PostMapping("/api/img/image")
    @ResponseStatus(HttpStatus.CREATED)
    public String uploadImage(@RequestParam MultipartFile multipartImage) throws Exception {
        String persisted_image = imageService.saveImage(multipartImage.getBytes(), multipartImage.getOriginalFilename());
        return "Persisted the following image:  " + persisted_image;
    }


    //@Secured({ "ROLE_WWWEASEL" })
    @DeleteMapping("/api/img/image/{imageName}")
    public void deleteImageByName(@PathVariable String imageName ){
        this.imageService.deleteImageByName( imageName );
    }

}

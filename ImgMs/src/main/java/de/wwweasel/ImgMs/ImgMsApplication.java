package de.wwweasel.ImgMs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImgMsApplication{

	public static void main(String[] args) {
		SpringApplication.run(ImgMsApplication.class, args);
	}

}

package de.wwweasel.ImgMs.service;

import de.wwweasel.ImgMs.dto.PersonEventDTO;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.reactive.function.client.WebClient;

public class RabbitConsumer {


    @Autowired
    private WebClient webClient;

    @Autowired
    private ImageService imageService;

    @RabbitListener(queues="imgLoginQueue")
    public void receive(PersonEventDTO dto) {

        if( this.imageService.findByEmailInDB(dto.getEmail()).isEmpty() ){
            System.out.println("Will download Avatar.");
            byte[] bytesvg = webClient.get()
                    .uri("/api/pixel-art/" + dto.getEmail() + ".svg?background=white")
                    //.accept(MediaType.valueOf("image/svg+xml"))
                    .retrieve()
                    .bodyToMono(byte[].class)
                    .block();

            if(bytesvg!=null){
                String persisted_image = this.imageService.saveImage( bytesvg, dto.getEmail()+".svg");
            }

        }else{
            System.out.println("User already has an Avatar.");
        }

    }

}

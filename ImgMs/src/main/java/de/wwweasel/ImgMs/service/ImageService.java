package de.wwweasel.ImgMs.service;

import de.wwweasel.ImgMs.domain.Image;
import de.wwweasel.ImgMs.repo.FileSystemRepository;
import de.wwweasel.ImgMs.repo.ImageRepository;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;


@Service
public class ImageService {

    private FileSystemRepository fileSystemRepository;
    private ImageRepository imageRepository;

    public ImageService(FileSystemRepository fileSystemRepository, ImageRepository imageRepository){
        this.fileSystemRepository = fileSystemRepository;
        this.imageRepository = imageRepository;
    }

    // Saves Image in FileSystemRepo as well as ImageRepository
    public String saveImage(byte[] bytes, String imageName) {

        return this.imageRepository.findByName( imageName )
                .map( image -> "Image with name:" + image.getLocation() + " already exists in the database." )
                .orElseGet( () -> {
                    String imagePath = null;
                    try {
                        imagePath = this.fileSystemRepository.save(bytes, imageName);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    String location = imageRepository.save(new Image(imageName, imagePath)).getLocation();
                    return "Persisted image: " + location + " in the database. ";
                } );

//        Optional<Image> imageOpt = this.imageRepository.findByName(imageName);
//        if(imageOpt.isPresent()){
//            return "Image with name:" + imageOpt.get().getLocation() + " already exists in the database.";
//        }else{
//            String imagePath = this.fileSystemRepository.save(bytes, imageName);
//            String location = imageRepository.save(new Image(imageName, imagePath)).getLocation();
//            return "Persisted image: " + location + " in the database. ";
//        }
    }

    // Search for image by name in imageRepo and if found stream it from FileSystem to Client
    public FileSystemResource findByEmail(String email) {
        Image image = imageRepository.findByEmail(email)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        return fileSystemRepository.findInFileSystem(image.getLocation());
    }

    // this one is called by the RabbitConsumer only
    // It checks if only the email is contained in the filename, without the extension( .svg/.jpg )
    public Optional<Image> findByEmailInDB(String email){
        return this.imageRepository.findByEmail( email );
    }


    // Search for id in imageRepo and if found stream it from FileSystem to Client
    public FileSystemResource findById(Long imageId) {
        Image image = imageRepository.findById(imageId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        return fileSystemRepository.findInFileSystem(image.getLocation());
    }

    public void deleteImageByName(String imageName){
        Image image = imageRepository.findByName( imageName )
                .orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND) );
        this.fileSystemRepository.delete( image.getLocation() );
    }

//    public String generateRandomString(int length, boolean useLetters, boolean useNumbers){
//        // Apache Commons Lang
//        return RandomStringUtils.random(length, useLetters, useNumbers);
//    }


}

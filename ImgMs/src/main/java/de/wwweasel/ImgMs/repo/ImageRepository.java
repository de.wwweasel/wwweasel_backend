package de.wwweasel.ImgMs.repo;

import de.wwweasel.ImgMs.domain.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ImageRepository extends JpaRepository<Image, Long> {

    @Query( value = "Select i.* " +
            "From image as i " +
            "where i.name = :name " +
            ";", nativeQuery=true )
    Optional<Image> findByName(String name);

    @Query( value = "Select i.* " +
            "From image as i " +
            "where i.name like :email% " +
            ";", nativeQuery=true )
    Optional<Image> findByEmail(String email);



}

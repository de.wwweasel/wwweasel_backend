package de.wwweasel.ImgMs.repo;

import de.wwweasel.ImgMs.config.Variables;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Repository
public class FileSystemRepository {

    /*  Since the ImageFolder is inside Docker and outside of it differently,
        you have to pass the Path to the config.file via EnvVariables.
     */

    private static Variables variables;
    private static Path img_folder_path;


    public FileSystemRepository(Variables variables) {
        this.variables = variables;
        this.img_folder_path = Path.of( this.variables.getProject_root_folder(),"/Images" );
    }

    public String save(byte[] content, String imageName) throws Exception {
        if(!Files.exists(img_folder_path)){
            Files.createDirectory(img_folder_path);
        }

        Path newFile = Path.of( this.img_folder_path.toString(), imageName );
        Files.write(newFile, content);
        return newFile.toAbsolutePath().toString();
    }

    public FileSystemResource findInFileSystem(String location) {
        try {
            return new FileSystemResource(Paths.get(location));
        } catch (Exception e) {
            // Handle access or file not found problems.
            throw new RuntimeException();
        }
    }

    public void delete(String location){
        if(this.findInFileSystem( location ) != null){
            try {
                Files.delete(Path.of(location));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}

package de.wwweasel.ImgMs.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.oauth2.server.resource.OAuth2ResourceServerConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@EnableWebSecurity
public class SecurityConfig implements WebMvcConfigurer {

    @Bean
    SecurityFilterChain filterChain(HttpSecurity http) throws Exception{
        return
                http
                        .cors()
                        .and()
                        .authorizeRequests(authorize -> authorize
                                //.mvcMatchers(HttpMethod.GET, "/chat/hello").permitAll()
                                .anyRequest().hasRole("PERSON")
                        )
                        .oauth2ResourceServer(OAuth2ResourceServerConfigurer::jwt)
                        .sessionManagement( sessionManagement ->
                                sessionManagement.sessionCreationPolicy(SessionCreationPolicy.STATELESS) )
                        .build();

    }

    /*
        The following Bean extracts the list of Roles from the AccessToken
        and sets the claim 'name' to email instead of subject
     */
    @Bean
    public JwtAuthenticationConverter jwtAuthenticationConverter() {
        var jwtGrantedAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();
        jwtGrantedAuthoritiesConverter.setAuthorityPrefix("ROLE_");
        jwtGrantedAuthoritiesConverter.setAuthoritiesClaimName("roles");

        var jwtAuthenticationConverter = new JwtAuthenticationConverter();
        jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(jwtGrantedAuthoritiesConverter);
        jwtAuthenticationConverter.setPrincipalClaimName("email"); // This will return the email-claim instead of the subject( UserId form Keycloak )
        return jwtAuthenticationConverter;
    }

    @Value("${cors_url}")
    private String cors_url;
    @Value("${cors_methods}")
    private String[] cors_methods;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins(cors_url)
                .allowedMethods(cors_methods)
        ;
    }
}

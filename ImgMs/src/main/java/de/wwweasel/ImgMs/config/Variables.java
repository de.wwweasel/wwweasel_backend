package de.wwweasel.ImgMs.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "variables")
public class Variables {

    private String project_root_folder;

    public String getProject_root_folder() {
        return project_root_folder;
    }

    public void setProject_root_folder(String project_root_folder) {
        this.project_root_folder = project_root_folder;
    }
}

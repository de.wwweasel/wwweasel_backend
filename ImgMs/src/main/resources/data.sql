DROP TABLE IF EXISTS public.image;

CREATE TABLE public.image (
  id BIGSERIAL PRIMARY KEY,
  location VARCHAR(255) NOT NULL,
  name VARCHAR(255) UNIQUE NOT NULL
);


INSERT INTO
    public.image (location, name)
VALUES
    ('/home/imgms/Images/weasel@tutamail.com.jpg', 'weasel@tutamail.com.jpg')
    ;
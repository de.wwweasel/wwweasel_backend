package de.wwweasel.ImgMs.controller;

import de.wwweasel.ImgMs.config.Variables;
import de.wwweasel.ImgMs.service.ImageService;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.ResourceUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Path;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest
@AutoConfigureMockMvc
@ActiveProfiles("production")
@ContextConfiguration(classes = {Variables.class, Controller.class}) // Adds @ConfigurationProperties to TestApplicationContext
class ControllerTest {

//    @Autowired
//    MockMvc mockMvc;
//    @MockBean
//    ImageService imageService;
//
//    @Test
//    void imageByEmail_unauthenticated() throws Exception{
//        mockMvc.perform(get("/"))
//                .andExpect(status().isUnauthorized());
//    }
//
//    @Test
//    @DisplayName("Should return an image from a email")
//    void imageByEmail_authtenticated() throws Exception{
//        File file = ResourceUtils.getFile("classpath:ha@ha.com.svg");
//        FileSystemResource img = new FileSystemResource(file);
//
//        Mockito.when( this.imageService.findByEmail( any(String.class) ) ).thenReturn( img );
//
//        mockMvc.perform(MockMvcRequestBuilders
//                    .get("/api/img/image/{email}", "ni@ni.com")
//                    .with(SecurityMockMvcRequestPostProcessors.user("nobody"))
//                    //.with(SecurityMockMvcRequestPostProcessors.csrf())
//                    // Theres no need to test csrf because at a ResourceSErver its disabled.
//                )
//                //.andDo(print())
//                .andExpect(status().isOk());
//
//    }
//
//
//    @Test
//    void saveEmail_unauthenticated() throws Exception{
//        mockMvc.perform(get("/api/img/image"))
//                .andExpect(status().isUnauthorized());
//    }
//
//
//    @Test
//    void saveEmail() throws Exception{
//
//        byte[] mockImage = IOUtils.toByteArray(getClass().getClassLoader().getResourceAsStream("ha@ha.com.svg"));
//
//        MockMultipartFile mockMultipartFile = new MockMultipartFile( "multipartImage", mockImage );
//
//        Mockito.when( this.imageService.saveImage( any(byte[].class), any(String.class) ) ).thenReturn( "mockImage" );
//
//        MvcResult mvcResult = mockMvc.perform( MockMvcRequestBuilders
//                .multipart("/api/img/image").file(mockMultipartFile)
//                .with(SecurityMockMvcRequestPostProcessors.user("nobody").roles("WWWEASEL"))
//                .with(SecurityMockMvcRequestPostProcessors.csrf())
//                )
//                .andExpect(status().isCreated())
//                .andReturn();
//
//
//        // Store the Response into a String
//        String responseBody = mvcResult.getResponse().getContentAsString();
//        // Deserialize the Pojo( outputGreet ) into a String and compare it with the ResponseString!
//        Assertions.assertEquals( responseBody, "Persisted the following image:  " + "mockImage" );
//
//    }//.multipart("/api/img/image").file(mockMultipartFile)
}
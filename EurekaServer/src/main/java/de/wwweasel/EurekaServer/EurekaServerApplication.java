package de.wwweasel.EurekaServer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;


@SpringBootApplication
@EnableEurekaServer
public class EurekaServerApplication implements ApplicationRunner {

	public static void main(String[] args) {
		SpringApplication.run(EurekaServerApplication.class, args);
	}

	@Value("${eureka_basicauth_username:default_shit}")
	private String eureka_basicauth_username;
	@Value("${test:default_test}")
	private String test;


	@Override
	public void run(ApplicationArguments args) throws Exception {
		System.out.println("vaultTest: " + test);
		System.out.println("eureka_basicauth_username: " + eureka_basicauth_username );
	}
}

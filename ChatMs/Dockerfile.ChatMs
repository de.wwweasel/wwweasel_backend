FROM maven:3.8-openjdk-11 as MAVEN_BUILD
COPY ChatMs/ /tmp/
WORKDIR /tmp/
#VOLUME $HOME/.m2 /root/.m2
RUN mvn clean package

FROM adoptopenjdk/openjdk11:jdk-11.0.11_9-alpine as default
RUN addgroup --gid 1000 -S chatms
RUN adduser -S chatms -u 1000 -G chatms
USER chatms
COPY --from=MAVEN_BUILD /tmp/configuration/target/configuration-0.0.1-SNAPSHOT.jar /usr/src/myapp/ChatMs.jar
ENTRYPOINT ["java","-jar","/usr/src/myapp/ChatMs.jar"]
EXPOSE 8082



FROM adoptopenjdk/openjdk11:jdk-11.0.11_9-alpine as staging
# Store IntermediateCA in JAVA_HOME TrustStore
ADD CONFIG/Vault/TLS_Certs/IntermediateCA/intermediateCA.pem $JAVA_HOME/lib/security/intermediateCA.pem
RUN keytool -importcert -alias intermediateCA -noprompt -file $JAVA_HOME/lib/security/intermediateCA.pem -cacerts -storepass changeit
ENV CHATMS_KEYSTORE=$JAVA_HOME/lib/security/chat.p12

RUN addgroup --gid 1000 -S chatms
RUN adduser -S chatms -u 1000 -G chatms
USER chatms
COPY --from=MAVEN_BUILD /tmp/configuration/target/configuration-0.0.1-SNAPSHOT.jar /usr/src/myapp/ChatMs.jar
ENTRYPOINT ["java","-jar","/usr/src/myapp/ChatMs.jar"]
EXPOSE 8082


FROM adoptopenjdk/openjdk11:jdk-11.0.11_9-alpine as production
# Store IntermediateCA in JAVA_HOME TrustStore
ADD CONFIG/Vault/TLS_Certs/IntermediateCA/intermediateCA.pem $JAVA_HOME/lib/security/intermediateCA.pem
RUN keytool -importcert -alias intermediateCA -noprompt -file $JAVA_HOME/lib/security/intermediateCA.pem -cacerts -storepass changeit
ENV CHAT_KEYSTORE=file:$JAVA_HOME/lib/security/chat.p12

ADD CONFIG/Letsencypt/auth-haberpursch-dev-chain.pem $JAVA_HOME/lib/security/auth-haberpursch-dev-chain.pem
ADD CONFIG/Letsencypt/haberpursch-dev-chain.pem $JAVA_HOME/lib/security/haberpursch-dev-chain.pem
RUN keytool -importcert -alias auth -noprompt -file $JAVA_HOME/lib/security/auth-haberpursch-dev-chain.pem -cacerts -storepass changeit
RUN keytool -importcert -alias haberpursch -noprompt -file $JAVA_HOME/lib/security/haberpursch-dev-chain.pem -cacerts -storepass changeit

RUN addgroup --gid 1000 -S chatms
RUN adduser -S chatms -u 1000 -G chatms
USER chatms
COPY --from=MAVEN_BUILD /tmp/configuration/target/configuration-0.0.1-SNAPSHOT.jar /usr/src/myapp/ChatMs.jar
ENTRYPOINT ["java","-jar","/usr/src/myapp/ChatMs.jar"]
EXPOSE 8082

package www.webAdapters;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import www.domain.Chatroom;
import www.domain.Message;

@Mapper(componentModel = "spring")
public abstract class SaveMessageWebMapper {

    // You can pass the Chatroom, because you pass the ChatroomId directly to saveMessage in the Controller
    @Mapping(target ="chatroom", ignore = true)
    abstract Message dtoToMessage( MessageWebDTO dto );


    //////////////////////////////////////////////////////////////////////////////////////////


    @Mapping(source = "chatroom", target = "chatroom_id", qualifiedByName = "chatroomToId")
    abstract MessageWebDTO messageToDto(Message message);

    @Named("chatroomToId")
    protected Long chatroomToId(Chatroom chatroom){
        return chatroom.getId();
    }

}

package www.webAdapters;

import org.mapstruct.*;
import www.domain.Chatroom;
import www.domain.Message;
import www.domain.Person;
import www.domain.Role;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface PersonByEmailWebMapper {

    @Mapping(source = "roles", target = "roles", qualifiedByName = "roles")
    PersonWebDTO personToDto(Person person);
    // This method call the following methods

    // Roles
    @Named("roles")
    default List<String> roles(List<Role> roles ){
        return roles.stream()
                .map( role -> role.getName())
                .collect(Collectors.toCollection(ArrayList::new));
    }


    @Mapping(source = "persons", target = "persons", qualifiedByName = "personsChatroomWebDTO")
    ChatroomWebDTO chatroomToDto(Chatroom chatroom);

    @Named("personsChatroomWebDTO")
    default List<String> personsChatroomWebDTO(List<Person> persons ){
        return persons.stream()
                .map( person -> person.getEmail())
                .collect(Collectors.toCollection(ArrayList::new));
    }

    // MessageWebDTO
    @Mapping(target = "chatroom_id", ignore = true)
    MessageWebDTO messageToDto( Message message );


    ///////////////////////////////////////////////////////////////////////////////////////////////

}

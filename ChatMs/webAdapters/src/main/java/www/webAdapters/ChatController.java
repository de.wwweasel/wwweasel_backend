package www.webAdapters;

import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import www.application.port.in.ChatsByEmailUseCase;
import www.application.port.in.SaveMessageUseCase;
import java.util.HashSet;


@RestController
class ChatController {


    private final SaveMessageUseCase saveMessageUseCase;
    private final ChatsByEmailUseCase chatsByEmailUseCase;
    private final SaveMessageWebMapper saveMessageWebMapper;
    private final ChatsByEmailWebMapper chatsByEmailWebMapper;

    ChatController( SaveMessageUseCase saveMessageUseCase, SaveMessageWebMapper saveMessageWebMapper, ChatsByEmailUseCase chatsByEmailUseCase, ChatsByEmailWebMapper chatsByEmailWebMapper) {
        this.saveMessageUseCase = saveMessageUseCase;
        this.saveMessageWebMapper = saveMessageWebMapper;
        this.chatsByEmailUseCase = chatsByEmailUseCase;
        this.chatsByEmailWebMapper = chatsByEmailWebMapper;
    }

    @Secured({"ROLE_PERSON"})
    @GetMapping("/api/chat/chatsByEmail" )
    public HashSet<ChatroomWebDTO> chatsByEmail(Authentication authentication){
        return this.chatsByEmailWebMapper.chatroomsToDtos(chatsByEmailUseCase.chatsByEmail(authentication.getName()));
    }

    @Secured({ "ROLE_PERSON" })
    @PostMapping("/api/chat/message")
    @ResponseStatus(HttpStatus.CREATED)
    public MessageWebDTO saveMessage(@RequestBody MessageWebDTO messageWebDTO){
        return this.saveMessageWebMapper.messageToDto(
                this.saveMessageUseCase.saveMessage(
                this.saveMessageWebMapper.dtoToMessage(messageWebDTO), messageWebDTO.getChatroom_id() ) );
    }

//    @GetMapping("/api/chat/hello" )
//    public String hello(){
//        return "Hello from Chat!";
//    }
//
//    @GetMapping("/api/chat/p")
//    public String principal(Principal principal, Authentication authentication){
//        return authentication.getName();
//    }

}

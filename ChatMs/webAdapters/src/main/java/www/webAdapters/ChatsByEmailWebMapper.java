package www.webAdapters;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import www.domain.Chatroom;
import www.domain.Message;
import www.domain.Person;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;


@Mapper(componentModel = "spring")
public interface ChatsByEmailWebMapper {

    HashSet<ChatroomWebDTO> chatroomsToDtos(HashSet<Chatroom> chatrooms);

    @Mapping(source = "persons", target = "persons", qualifiedByName = "personsChatroomWebDTO")
    ChatroomWebDTO chatroomToDto(Chatroom chatroom);

    @Named("personsChatroomWebDTO")
    default List<String> personsChatroomWebDTO(List<Person> persons ){
        return persons.stream()
                .map( person -> person.getEmail())
                .collect(Collectors.toCollection(ArrayList::new));
    }

    // MessageWebDTO
    @Mapping(target = "chatroom_id", ignore = true)
    MessageWebDTO messageToDto( Message message );

}

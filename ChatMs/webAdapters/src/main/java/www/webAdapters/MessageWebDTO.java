package www.webAdapters;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.Objects;

class MessageWebDTO{

    private Long id;
    private String message;
    private Long chatroom_id; // Cannot be NULL !
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime creationdate;
    private String email;

    public MessageWebDTO() { }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public Long getChatroom_id() {
        return chatroom_id;
    }
    public void setChatroom_id(Long chatroom_id) {
        this.chatroom_id = chatroom_id;
    }
    public LocalDateTime getCreationdate() {
        return creationdate;
    }
    public void setCreationdate(LocalDateTime creationdate) {
        this.creationdate = creationdate;
    }
    public String getEmail() { return email; }
    public void setEmail(String email) { this.email = email; }

    @Override
    public String toString() {
        return "MessageDTO{" +
                "id=" + id +
                ", message='" + message + '\'' +
                ", chatroom_id=" + chatroom_id +
                ", creationdate=" + creationdate +
                ", useremail='" + email + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessageWebDTO that = (MessageWebDTO) o;
        return Objects.equals(creationdate, that.creationdate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(creationdate);
    }

}

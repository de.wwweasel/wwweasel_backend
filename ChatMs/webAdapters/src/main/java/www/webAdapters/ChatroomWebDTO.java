package www.webAdapters;

import java.util.*;

class ChatroomWebDTO {

    private Long id;
    private String name;
    private List<MessageWebDTO> messages = new ArrayList<>();
    private List<String> persons = new ArrayList<>();


    public ChatroomWebDTO(){ }

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }
    public String getName() { return name; }
    public void setName(String name) { this.name = name; }
    public List<MessageWebDTO> getMessages() { return messages; }
    public void setMessages(List<MessageWebDTO> messages) { this.messages = messages; }
    public List<String> getPersons() { return persons; }
    public void setPersons(List<String> persons) { this.persons = persons; }

    @Override
    public String toString() {
        return "ChatroomDTO{" +
                "chatroom_id=" + id +
                ", chatroom_name='" + name + '\'' +
                ", messages=" + messages +
                ", persons=" + persons +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChatroomWebDTO that = (ChatroomWebDTO) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

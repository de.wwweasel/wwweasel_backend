package www.webAdapters.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseStatus;
import www.application.exceptions.ChatroomNotFoundException;
import www.application.exceptions.PersonNotFoundException;

import javax.validation.ConstraintViolationException;
import java.util.NoSuchElementException;


@ControllerAdvice
public class ExceptionHandler {

    // The following catches MethodParameterValidationExceptions
    @org.springframework.web.bind.annotation.ExceptionHandler(value = {MethodArgumentNotValidException.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ResponseEntity<ExceptionDTO> handleMethodArgumentNotValidException(MethodArgumentNotValidException e){
        ExceptionDTO exceptionDTO = new ExceptionDTO( HttpStatus.BAD_REQUEST.value(), e.getMessage() );

        for(FieldError fieldError:e.getBindingResult().getFieldErrors()){
            exceptionDTO.addValidationError( fieldError.getField(), fieldError.getDefaultMessage() );
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exceptionDTO);
    }

    // The following catches BeanValidationExceptions for fields in classes
    @org.springframework.web.bind.annotation.ExceptionHandler(value = {ConstraintViolationException.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ResponseEntity<ExceptionDTO> handleConstraintViolationException(ConstraintViolationException e){
        ExceptionDTO exceptionDTO = new ExceptionDTO( HttpStatus.BAD_REQUEST.value(), e.getMessage() );

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exceptionDTO);
    }

    // This intercepts if the if you try to add a Message to a Chatroom that does not exist.
    @org.springframework.web.bind.annotation.ExceptionHandler(value = { ChatroomNotFoundException.class })
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ExceptionDTO> handleNoSuchElementException( ChatroomNotFoundException e ){
        ExceptionDTO exceptionDTO = new ExceptionDTO( HttpStatus.NOT_FOUND.value(), "Could not find Chatroom with given ID. ");
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exceptionDTO);
    }

    // The following triggers if you you try to load a Person from the database that does not exist
    @org.springframework.web.bind.annotation.ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ExceptionDTO> handlePersonNotFoundException( PersonNotFoundException e ){
        ExceptionDTO exceptionDTO = new ExceptionDTO(HttpStatus.NOT_FOUND.value(), e.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exceptionDTO);
    }



}

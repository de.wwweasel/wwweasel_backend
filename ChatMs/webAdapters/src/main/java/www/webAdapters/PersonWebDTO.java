package www.webAdapters;

import java.util.*;

class PersonWebDTO {

    private Long id;
    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private String subject;
    private List<ChatroomWebDTO> chatrooms = new ArrayList<>();
    private List<String> roles = new ArrayList<>();

    public PersonWebDTO() { }

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public String getUsername() { return username; }
    public void setUsername(String username) { this.username = username; }

    public String getFirstName() { return firstName; }
    public void setFirstName(String firstName) { this.firstName = firstName; }

    public String getLastName() { return lastName; }
    public void setLastName(String lastName) { this.lastName = lastName; }

    public String getEmail() { return email; }
    public void setEmail(String email) { this.email = email; }

    public String getSubject() { return subject; }
    public void setSubject(String subject) { this.subject = subject; }

    public List<ChatroomWebDTO> getChatrooms() { return chatrooms; }
    public void setChatrooms(List<ChatroomWebDTO> chatrooms) { this.chatrooms = chatrooms; }
    public List<String> getRoles() { return roles; }
    public void setRoles(List<String> roles) { this.roles = roles; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonWebDTO that = (PersonWebDTO) o;
        return Objects.equals(email, that.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email);
    }

    @Override
    public String toString() {
        return "PersonWebDTO{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", subject='" + subject + '\'' +
                ", chatrooms=" + chatrooms +
                ", roles=" + roles +
                '}';
    }
}

package www.webAdapters;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import www.application.port.in.ChatsByEmailUseCase;
import www.application.port.in.SaveMessageUseCase;
import www.domain.Chatroom;
import www.domain.Message;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.HashSet;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.jwt;


@WebMvcTest(controllers = {ChatController.class}, properties = {"spring.cloud.config.enabled=false"})
@AutoConfigureMockMvc
@ActiveProfiles("production")
class ChatControllerTest {


    private ChatController chatController;
    @Autowired
    private SaveMessageWebMapper saveMessageWebMapper;
    @Autowired
    private ChatsByEmailWebMapper chatsByEmailWebMapper;
    @MockBean
    private ChatsByEmailUseCase chatsByEmailUseCase;
    @MockBean
    private SaveMessageUseCase saveMessageUseCase;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    ObjectMapper objectMapper;
    private final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();


    @BeforeEach
    void setupPorts(){
        chatController = new ChatController(saveMessageUseCase, saveMessageWebMapper, chatsByEmailUseCase, chatsByEmailWebMapper);
    }


    @Test
    public void returnsUnauthorized_withNoAuth() throws Exception {
        this.mockMvc.perform(get("/api/chat/chatsByEmail")).andExpect(status().isUnauthorized());
    }


    @Test
    void chatsByEmail() throws Exception {

        Chatroom chat_1 = new Chatroom("chat_1");
        chat_1.setId(1L);
        Chatroom chat_2 = new Chatroom("chat_2");
        chat_1.setId(2L);
        HashSet<Chatroom> chatrooms = new HashSet<>();
        chatrooms.add(chat_1);
        chatrooms.add(chat_2);

        Mockito.when( this.chatsByEmailUseCase.chatsByEmail( any(String.class) ) ).thenReturn( chatrooms );

        mockMvc.perform( MockMvcRequestBuilders
                        .get("/api/chat/chatsByEmail")
                        //.with(SecurityMockMvcRequestPostProcessors.user("nobody").roles("admin"))
                        //.with(SecurityMockMvcRequestPostProcessors.csrf())
                        .with( jwt().authorities( new SimpleGrantedAuthority("PERSON") ) )
                )
                //.andDo(print())
                .andExpect(status().isOk())
                //.andExpect(jsonPath("$.email").value("ma@ma.com"))
                //.andExpect(jsonPath("$.id").isNumber())
                ;
    }

    @Test
    public void saveMessage_returnsUnauthorized() throws Exception {
        this.mockMvc.perform(get("/api/chat/message")).andExpect(status().isUnauthorized());
    }

    @Test
    void saveMessage() throws Exception{
        MessageWebDTO dto = new MessageWebDTO();
        dto.setMessage("TestMessage!");
        dto.setChatroom_id(7L);
        dto.setEmail("ni@ni.com");

        Message message = this.saveMessageWebMapper.dtoToMessage(dto);
        Chatroom chatroom = new Chatroom("TestChat");
        chatroom.setId(7L);
        chatroom.getMessages().add(message);
        message.setChatroom( chatroom );

        Mockito.when( this.saveMessageUseCase.saveMessage(message, dto.getChatroom_id()) ).thenReturn(message);

        MvcResult mvcResult = mockMvc.perform( MockMvcRequestBuilders
                .post("/api/chat/message")
                .with(SecurityMockMvcRequestPostProcessors.user("nobody").roles("PERSON"))
                .with(SecurityMockMvcRequestPostProcessors.csrf())
                .contentType(MediaType.APPLICATION_JSON)// Incoming following JSON
                .content(objectMapper.writeValueAsString(dto)) ) // serialize above declared MessageWebDTO to a JSON
                .andExpect(status().isCreated())// status
                .andReturn(); // This return the Http response which should be a JSON of MessageWebDTO

        String responseBody = mvcResult.getResponse().getContentAsString();
        System.out.println("responseBody: " + responseBody);
        //Assertions.assertEquals( responseBody, this.objectMapper.writeValueAsString(dto) );
    }

}
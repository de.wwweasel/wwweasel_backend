package www.domain;

import javax.validation.constraints.*;
import java.time.LocalDateTime;
import java.util.Objects;

public class Message extends SelfValidating<Message>{

    private Long id;
    @NotBlank(message = "Message cannot be empty")
    private String message;
    @NotNull
    private Chatroom chatroom;
    // Will be created at persistence
    private LocalDateTime creationdate;
    @Email(message = "${validatedValue} is not a valid email")
    @NotBlank(message = "Email cannot be empty")
    private String email;

    public Message(){}
    public Message(Long id, String message, Chatroom chatroom, LocalDateTime creationdate, String email) {
        this.id = id;
        this.message = message;
        this.chatroom = chatroom;
        this.creationdate = creationdate;
        this.email = email;

        this.validateSelf();
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public Chatroom getChatroom() { return chatroom; }
    public void setChatroom(Chatroom chatroom) { this.chatroom = chatroom; }
    public LocalDateTime getCreationdate() {
        return creationdate;
    }
    public void setCreationdate(LocalDateTime creationdate) {
        this.creationdate = creationdate;
    }
    public String getEmail() { return email; }
    public void setEmail(String email) { this.email = email; }


    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", message='" + message + '\'' +
                ", creationdate=" + creationdate +
                ", email='" + email + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return Objects.equals(creationdate, message.creationdate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(creationdate);
    }
}

package www.domain;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.*;

public class Person extends SelfValidating<Person> {

    private Long id;
    private String username;
    private String firstName;
    private String lastName;
    @Email(message = "${validatedValue} is not a valid email")
    @NotBlank(message = "Email cannot be empty")
    private String email;
    private String subject; // Keycloak's UUID for the user
    private List<Chatroom> chatrooms = new ArrayList<>();
    private List<Role> roles = new ArrayList<>();

    public Person() {}

    public Person(Long id, String username, String firstName, String lastName, String email, String subject) {
        this.id = id;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.subject = subject;
    }

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public String getUsername() { return username; }
    public void setUsername(String username) { this.username = username; }

    public String getFirstName() { return firstName; }
    public void setFirstName(String firstName) { this.firstName = firstName; }

    public String getLastName() { return lastName; }
    public void setLastName(String lastName) { this.lastName = lastName; }

    public String getEmail() { return email; }
    public void setEmail(String email) { this.email = email; }

    public String getSubject() { return subject; }
    public void setSubject(String subject) { this.subject = subject; }

    public List<Chatroom> getChatrooms() { return chatrooms; }
    public void setChatrooms(List<Chatroom> chatrooms) { this.chatrooms = chatrooms; }

    public List<Role> getRoles() { return roles; }
    public void setRoles(List<Role> roles) { this.roles = roles; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(email, person.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email);
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", subject='" + subject + '\'' +
                ", chatrooms=" + chatrooms +
                ", roles=" + roles +
                '}';
    }
}

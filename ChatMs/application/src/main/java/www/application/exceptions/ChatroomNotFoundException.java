package www.application.exceptions;

public class ChatroomNotFoundException extends RuntimeException{

    public ChatroomNotFoundException(String message){
        super(message);
    }
}

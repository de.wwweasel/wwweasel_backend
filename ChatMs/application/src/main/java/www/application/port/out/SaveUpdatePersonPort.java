package www.application.port.out;


import www.domain.Person;

public interface SaveUpdatePersonPort {

    Person saveUpdatePerson(Person person);
}

package www.application.port.out;

import www.domain.Person;
import java.util.Optional;


public interface PersonByEmailPort {

    Optional<Person> personByEmail(String email );

}

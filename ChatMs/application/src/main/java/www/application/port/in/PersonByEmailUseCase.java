package www.application.port.in;

import www.domain.Person;

public interface PersonByEmailUseCase {
    Person personByEmail(String email);
}

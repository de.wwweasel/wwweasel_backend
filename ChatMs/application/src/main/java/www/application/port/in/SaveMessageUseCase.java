package www.application.port.in;

import www.domain.Message;

public interface SaveMessageUseCase {

    Message saveMessage(Message message, Long chatroom_id );

}

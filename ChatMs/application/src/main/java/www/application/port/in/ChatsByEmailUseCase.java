package www.application.port.in;

import www.domain.Chatroom;

import java.util.HashSet;

public interface ChatsByEmailUseCase {
    HashSet<Chatroom> chatsByEmail(String email);
}

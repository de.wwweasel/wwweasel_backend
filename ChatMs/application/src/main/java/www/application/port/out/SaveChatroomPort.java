package www.application.port.out;

import www.domain.Chatroom;


public interface SaveChatroomPort {

    Chatroom saveChatroom(Chatroom chatroom );

}

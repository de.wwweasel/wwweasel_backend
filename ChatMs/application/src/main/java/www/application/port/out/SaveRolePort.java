package www.application.port.out;

import www.domain.Role;

public interface SaveRolePort {

    Role saveRole(Role role );

}

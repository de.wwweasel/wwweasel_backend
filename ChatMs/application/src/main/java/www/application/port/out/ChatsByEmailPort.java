package www.application.port.out;


import www.domain.Chatroom;

import java.util.HashSet;

public interface ChatsByEmailPort {

    HashSet<Chatroom> chatsByEmail(String email );

}

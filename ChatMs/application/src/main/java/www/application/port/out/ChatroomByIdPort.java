package www.application.port.out;

import www.domain.Chatroom;

import java.util.Optional;

public interface ChatroomByIdPort {

    Optional<Chatroom> chatroomById(Long id);

}

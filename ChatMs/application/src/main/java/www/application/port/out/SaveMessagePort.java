package www.application.port.out;

import www.domain.Message;

public interface SaveMessagePort {
    Message saveMessage( Message message );
}

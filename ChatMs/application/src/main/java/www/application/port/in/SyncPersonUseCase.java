package www.application.port.in;

import www.domain.Person;

public interface SyncPersonUseCase {
    boolean syncPerson(Person person );
}

package www.application.port.in;

import www.domain.Person;


public interface CreateDefaultChatUseCase {

    Person createDefaultChat( Person person );

}

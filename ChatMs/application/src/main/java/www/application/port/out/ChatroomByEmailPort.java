package www.application.port.out;

import www.domain.Chatroom;
import java.util.ArrayList;

public interface ChatroomByEmailPort {
    ArrayList<Chatroom> chatroomByEmail(String email);
}

package www.application.service;

import org.springframework.stereotype.Service;
import www.application.exceptions.PersonNotFoundException;
import www.application.port.in.PersonByEmailUseCase;
import www.application.port.out.PersonByEmailPort;
import www.domain.Person;
import javax.transaction.Transactional;


@Transactional
@Service
class PersonByEmailService implements PersonByEmailUseCase {

    private final PersonByEmailPort personByEmailPort;

    PersonByEmailService(PersonByEmailPort personByEmailPort) {
        this.personByEmailPort = personByEmailPort;
    }

    @Override
    public Person personByEmail(String email ) {
        return personByEmailPort.personByEmail( email )
                .orElseThrow( () -> new PersonNotFoundException("Could not get Person with email: ->  " + email) );
    }
}

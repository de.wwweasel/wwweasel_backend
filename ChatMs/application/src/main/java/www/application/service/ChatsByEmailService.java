package www.application.service;

import org.springframework.stereotype.Service;
import www.application.port.in.ChatsByEmailUseCase;
import www.application.port.out.ChatsByEmailPort;
import www.domain.Chatroom;

import javax.transaction.Transactional;
import java.util.HashSet;

@Transactional
@Service
public class ChatsByEmailService implements ChatsByEmailUseCase {

    private final ChatsByEmailPort chatsByEmailPort;

    public ChatsByEmailService(ChatsByEmailPort chatsByEmailPort) {
        this.chatsByEmailPort = chatsByEmailPort;
    }


    @Override
    public HashSet<Chatroom> chatsByEmail(String email) {
        return chatsByEmailPort.chatsByEmail( email );
    }
}

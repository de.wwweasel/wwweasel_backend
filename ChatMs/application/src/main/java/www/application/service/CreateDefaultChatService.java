package www.application.service;

import org.springframework.stereotype.Service;
import www.application.exceptions.PersonNotFoundException;
import www.application.port.in.CreateDefaultChatUseCase;
import www.application.port.out.PersonByEmailPort;
import www.application.port.out.SaveChatroomPort;
import www.application.port.out.SaveUpdatePersonPort;
import www.domain.Chatroom;
import www.domain.Person;

import javax.transaction.Transactional;

@Service
@Transactional
public class CreateDefaultChatService implements CreateDefaultChatUseCase {

    private final PersonByEmailPort personByEmailPort;
    private final SaveUpdatePersonPort saveUpdatePersonPort;
    private final SaveChatroomPort saveChatroomPort;


    public CreateDefaultChatService(PersonByEmailPort personByEmailPort, SaveUpdatePersonPort saveUpdatePersonPort, SaveChatroomPort saveChatroomPort) {
        this.personByEmailPort = personByEmailPort;
        this.saveUpdatePersonPort = saveUpdatePersonPort;
        this.saveChatroomPort = saveChatroomPort;
    }

    @Override
    public Person createDefaultChat(Person person) {

        // Find wwweasel
        Person weasel = this.personByEmailPort.personByEmail( "weasel@tutamail.com" )
                .orElseThrow( () -> new PersonNotFoundException("Could not find wwweasel in the database!!!") );

        // Create a default Chatroom with wwweasel and Person
        Chatroom default_chatroom = new Chatroom( person.getUsername() + " | " + weasel.getUsername() );
        // Add wwweasel and person to deafult chatroom
        default_chatroom.getPersons().add( person );
        default_chatroom.getPersons().add( weasel );

        // Persist Chat
        Chatroom persisted_chatroom = saveChatroomPort.saveChatroom(default_chatroom);

        // Add default chatroom to person and wwweasel
        weasel.getChatrooms().add( persisted_chatroom );
        person.getChatrooms().add( persisted_chatroom );

        this.saveUpdatePersonPort.saveUpdatePerson(weasel);

        return person;

    }
}

package www.application.service;

import org.springframework.stereotype.Service;
import www.application.exceptions.ChatroomNotFoundException;
import www.application.port.in.SaveMessageUseCase;
import www.application.port.out.ChatroomByIdPort;
import www.application.port.out.SaveMessagePort;
import www.domain.Chatroom;
import www.domain.Message;
import javax.transaction.Transactional;

@Transactional
@Service
public class SaveMessageService implements SaveMessageUseCase {

    private final SaveMessagePort saveMessagePort;
    private final ChatroomByIdPort chatroomByIdPort;

    public SaveMessageService(SaveMessagePort saveMessagePort, ChatroomByIdPort chatroomByIdPort) {
        this.saveMessagePort = saveMessagePort;
        this.chatroomByIdPort = chatroomByIdPort;
    }

    @Override
    public Message saveMessage(Message message, Long chatroom_id ) {

        Chatroom chatroom = this.chatroomByIdPort.chatroomById(chatroom_id)
            .orElseThrow( () -> new ChatroomNotFoundException("Could not find Chatroom with ID: " + chatroom_id) );
        message.setChatroom(chatroom);
        return this.saveMessagePort.saveMessage( message );

    }
}
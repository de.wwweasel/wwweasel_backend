package www.application.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import www.application.exceptions.ChatroomNotFoundException;
import www.application.port.out.ChatroomByIdPort;
import www.application.port.out.SaveMessagePort;
import www.domain.Chatroom;
import www.domain.Message;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;


@ExtendWith(MockitoExtension.class)
class SaveMessageServiceTest {

    private SaveMessageService saveMessageService;
    @Mock
    private SaveMessagePort saveMessagePort;
    @Mock
    private ChatroomByIdPort chatroomByIdPort;

    private final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    @BeforeEach
    void setupPorts(){
        saveMessageService = new SaveMessageService(this.saveMessagePort, this.chatroomByIdPort);
    }

    @Test
    @DisplayName("Should save and return a Message with valid Chatroom")
    void saveMessageWithValidChatroom() {

        Message message = new Message();
        message.setId(34L);
        message.setMessage("TestMessage");
        message.setChatroom(new Chatroom("TestChatroom"));
        message.setEmail("weasel@tutamail.com");
        message.setCreationdate(LocalDateTime.now());

        // Make sure that Bean Validation has zero violations
        Set<ConstraintViolation<Message>> violations = validator.validate(message);
        Assertions.assertEquals(violations.size(),0);


        Optional<Chatroom> testChatroomOpt = Optional.of( new Chatroom("TestChatroom") );
        Mockito.when( this.chatroomByIdPort.chatroomById( any(Long.class) ) ).thenReturn( testChatroomOpt );// If its an Optional: Optional.of(testChatroom)

        Mockito.when(this.saveMessagePort.saveMessage(any(Message.class))).thenReturn(message);

        //
        Message savedMessage = saveMessageService.saveMessage(message, message.getId());

        Assertions.assertNotNull(savedMessage.getMessage());
        Assertions.assertEquals(savedMessage.getId(), message.getId());
        Assertions.assertEquals(savedMessage.getMessage(), message.getMessage());
    }


    @Test
    @DisplayName("Should throw an Exception because of a invalid Chatroom")
    void saveMessageWithInValidChatroom() {

        Message message = new Message();
        message.setId(364L);
        message.setMessage("TestMessage");
        message.setChatroom(new Chatroom("TestChatroom"));
        message.setEmail("weasel@tutamail.com");
        message.setCreationdate(LocalDateTime.now());

        // Make sure that Bean Validation has zero violations
        Set<ConstraintViolation<Message>> violations = validator.validate(message);
        Assertions.assertEquals(violations.size(),0);

        Optional<Chatroom> testChatroomOpt = Optional.empty();
        Mockito.when( this.chatroomByIdPort.chatroomById( any(Long.class) ) ).thenReturn( testChatroomOpt );// If its an Optional: Optional.of(testChatroom)

        //

        ChatroomNotFoundException chatroomNotFoundException = Assertions.assertThrows( ChatroomNotFoundException.class, () -> {
            Message savedMessage = saveMessageService.saveMessage(message, message.getId());
        }  );
        Assertions.assertTrue( chatroomNotFoundException.getMessage().contains("Could not find Chatroom with ID: " + message.getId()) );

    }
}
package www.application.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import www.application.port.out.ChatsByEmailPort;
import www.domain.Chatroom;
import www.domain.Message;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;


@ExtendWith(MockitoExtension.class)
class ChatsByEmailServiceTest {


    private ChatsByEmailService chatsByEmailService;
    @Mock
    private ChatsByEmailPort chatsByEmailPort;

    @BeforeEach
    void setupPorts(){
        chatsByEmailService = new ChatsByEmailService(this.chatsByEmailPort);
    }

    @Test
    @DisplayName("Should return Chatrooms from given email")
    void chatsByEmail() {

        Chatroom chat_1 = new Chatroom("Chat_1");
        Chatroom chat_2 = new Chatroom("Chat_2");
        chat_1.setId(1L);
        chat_1.setId(2L);

        Message messge_1 = new Message();
        messge_1.setId(1L);
        messge_1.setMessage("message_1");
        messge_1.setChatroom(chat_1);
        messge_1.setEmail("weasel@tutamail.com");
        messge_1.setCreationdate(LocalDateTime.now());

        Message messge_2 = new Message();
        messge_1.setId(1L);
        messge_1.setMessage("message_2");
        messge_1.setChatroom(chat_2);
        messge_1.setEmail("ni@ni.com");
        messge_1.setCreationdate(LocalDateTime.now());

        HashSet<Chatroom> set = new HashSet<>();
        set.add(chat_1);
        set.add(chat_2);

        Mockito.when( this.chatsByEmailPort.chatsByEmail( any(String.class) ) ).thenReturn( set );

        HashSet<Chatroom> result_chatrooms = chatsByEmailService.chatsByEmail("ma@ma.com");

        Assertions.assertNotNull(result_chatrooms);
        Assertions.assertEquals(result_chatrooms.size(),2);


    }

    @Test
    @DisplayName("Should return empty Hashset")
    void chatsByEmailEmpty() {

        Mockito.when( this.chatsByEmailPort.chatsByEmail( any(String.class) ) ).thenReturn( new HashSet<>() );

        HashSet<Chatroom> result_chatrooms = chatsByEmailService.chatsByEmail("ma@ma.com");

        Assertions.assertNotNull(result_chatrooms);
        Assertions.assertEquals(result_chatrooms.size(),0);


    }
}
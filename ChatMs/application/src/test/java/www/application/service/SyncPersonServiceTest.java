package www.application.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import www.application.exceptions.PersonNotFoundException;
import www.application.port.out.PersonByEmailPort;
import www.application.port.out.SaveChatroomPort;
import www.application.port.out.SaveUpdatePersonPort;
import www.domain.Chatroom;
import www.domain.Person;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Optional;
import java.util.Set;


@ExtendWith(MockitoExtension.class)
class SyncPersonServiceTest {

    private SyncPersonService syncPersonService;
    @Mock
    private PersonByEmailPort personByEmailPort;
    @Mock
    private SaveUpdatePersonPort saveUpdatePersonPort;
    @Mock
    private CreateDefaultChatService createDefaultChatService;
    @Captor
    private ArgumentCaptor<Chatroom> chatroomArgumentCaptor;
    @Captor
    private ArgumentCaptor<Person> personArgumentCaptor;

    private final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    @BeforeEach
    void setupPorts(){
        this.syncPersonService = new SyncPersonService(personByEmailPort, saveUpdatePersonPort, createDefaultChatService);
    }


    @Test
    @DisplayName("Should update given, already existing Person")
    void syncPerson_knownPerson() {

        Person person = new Person();
        person.setEmail("known@known.com");
        person.setId(123L);
        person.setUsername("known");
        Mockito.when( this.personByEmailPort.personByEmail(person.getEmail())).thenReturn(Optional.of( person ) );

        // Make sure that Bean Validation has zero violations
        Set<ConstraintViolation<Person>> violations = validator.validate(person);
        Assertions.assertEquals(violations.size(),0);

        //

        this.syncPersonService.syncPerson(person);

        //
        Mockito.verify( this.saveUpdatePersonPort, Mockito.times(1) ).saveUpdatePerson(personArgumentCaptor.capture());
        Person passedPerson = personArgumentCaptor.getValue();
        Assertions.assertEquals(passedPerson.getEmail(), person.getEmail());

    }

    @Test
    @DisplayName("Should create a default Chatroom and save given new Person to DB")
    void syncPerson_unknownPerson() {

        Person person = new Person();
        person.setEmail("unknown@unknown.com");
        person.setId(123L);
        person.setUsername("unknown");
        Mockito.when( this.personByEmailPort.personByEmail(person.getEmail())).thenReturn( Optional.empty() );

        // Make sure that Bean Validation has zero violations
        Set<ConstraintViolation<Person>> violations = validator.validate(person);
        Assertions.assertEquals(violations.size(),0);

        Mockito.when( this.saveUpdatePersonPort.saveUpdatePerson(person) ).thenReturn(person);
        Mockito.when( this.createDefaultChatService.createDefaultChat(person) ).thenReturn(person);
        //

        this.syncPersonService.syncPerson(person);

        // When personByEmailPort returns an empty Optional of Person, then the passed in Person should be persisted by triggering the savePersonPort
        Mockito.verify( this.saveUpdatePersonPort, Mockito.times(1)).saveUpdatePerson(this.personArgumentCaptor.capture());
        Person passedPerson = this.personArgumentCaptor.getValue();
        Assertions.assertEquals(passedPerson.getEmail(), person.getEmail());
    }

}
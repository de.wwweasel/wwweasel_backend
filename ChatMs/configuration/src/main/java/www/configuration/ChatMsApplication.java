package www.configuration;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import www.application.service.ApplicationConfig;
import www.eventConsumerAdapter.RabbitMQConfig;
import www.persistenceAdapters.PersistenceAdaptersConfig;
import www.webAdapters.WebAdaptersConfig;

@SpringBootApplication

//@EnableEurekaClient
@Import({ApplicationConfig.class, WebAdaptersConfig.class, PersistenceAdaptersConfig.class, RabbitMQConfig.class})
//@ComponentScan(basePackages = {"www"})
//@EnableJpaRepositories(basePackages = "www")
//@EntityScan(basePackages = "www")
public class ChatMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChatMsApplication.class, args);
	}

}

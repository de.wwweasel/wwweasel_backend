package www.eventConsumerAdapter;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import www.application.port.in.SyncPersonUseCase;

@Configuration
@ComponentScan
public class RabbitMQConfig {

    private final SyncPersonUseCase syncPersonUseCase;
    private final SyncPersonEventConsumerMapper syncPersonEventConsumerMapper;

    public RabbitMQConfig(SyncPersonUseCase syncPersonUseCase, SyncPersonEventConsumerMapper syncPersonEventConsumerMapper) {
        this.syncPersonUseCase = syncPersonUseCase;
        this.syncPersonEventConsumerMapper = syncPersonEventConsumerMapper;
    }


    @Bean
    public TopicExchange exchange() {
        return new TopicExchange("customExchange");
    }

    @Bean
    public Queue loginQueue() {
        return new Queue("loginQueue");
    }

    @Bean
    public Binding loginBinding(@Qualifier("loginQueue") Queue queue, TopicExchange topicExchange) {
        return BindingBuilder
                .bind(queue)
                .to(topicExchange)
                .with("login.*")
                ;
    }

    @Bean
    public MessageConverter messageConverter(){
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    AmqpTemplate amqpTemplate(ConnectionFactory connectionFactory){
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter( messageConverter() );
        return rabbitTemplate;
    }

    @Bean
    RabbitConsumer rabbitConsumer(){
        return new RabbitConsumer( this.syncPersonUseCase, this.syncPersonEventConsumerMapper);
    }

}

package www.eventConsumerAdapter;

import org.mapstruct.*;
import www.domain.Chatroom;
import www.domain.Message;
import www.domain.Person;
import www.domain.Role;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface SyncPersonEventConsumerMapper {

    // Ignore Chatrooms because there arent any
    @Mapping(target = "chatrooms", ignore = true)
    @Mapping(source = "roles", target = "roles", qualifiedByName = "stringToRole")
    Person eventDtoToPerson( PersonEventDTO eventDto);

    @Named("stringToRole")
    default List<Role> stringToRole(List<String> roles ){

        return roles.stream()
                .map( role -> new Role(role) )
                .collect(Collectors.toCollection(ArrayList::new));
    }

    // Now add the Person to the Roles with an @AfterMapping
    @AfterMapping
    default void personToRoles(@MappingTarget Person person){
        person.getRoles().stream()
                .map(role -> role.getPersons().add(person));
    }


    ///////////////////////////////////////////////////////////

}

package www.eventConsumerAdapter;


import org.springframework.amqp.rabbit.annotation.RabbitListener;
import www.application.port.in.SyncPersonUseCase;

public class RabbitConsumer {

    private final SyncPersonUseCase syncPersonUseCase;
    private final SyncPersonEventConsumerMapper mapper;

    public RabbitConsumer(SyncPersonUseCase syncPersonUseCase, SyncPersonEventConsumerMapper mapper) {
        this.syncPersonUseCase = syncPersonUseCase;
        this.mapper = mapper;
    }

    @RabbitListener(queues="loginQueue")
    public void receive(PersonEventDTO dto) {
        System.out.println("Will SAVE/UPDATE Person: " );
        System.out.println("  ->   " + dto);
        this.syncPersonUseCase.syncPerson( this.mapper.eventDtoToPerson(dto) );
    }

}

DROP TABLE IF EXISTS public.person_chatroom;
DROP TABLE IF EXISTS public.person_role;
DROP TABLE IF EXISTS public.person;
DROP TABLE IF EXISTS public.role;
DROP TABLE IF EXISTS public.message;
DROP TABLE IF EXISTS public.chatroom;


CREATE TABLE public.person (
  id BIGSERIAL PRIMARY KEY,
  username VARCHAR(255) DEFAULT 'Anonymous' NOT NULL,
  firstName VARCHAR(255) DEFAULT 'Anonymous' NOT NULL,
  lastName VARCHAR(255) DEFAULT 'Anonymous' NOT NULL,
  email VARCHAR(255) UNIQUE NOT NULL,
  subject VARCHAR(255) NOT NULL
);


CREATE TABLE public.chatroom (
  id BIGSERIAL PRIMARY KEY,
  name VARCHAR(255) DEFAULT 'Chat' NOT NULL
);


create table public.person_chatroom (
    person_id bigint not null,
    chatroom_id bigint not null,
    unique( person_id, chatroom_id ),
    constraint person_fk
        foreign key(person_id) references person(id),
    constraint chatroom_fk
        foreign key(chatroom_id) references chatroom(id)
);

CREATE TABLE public.role (
  id BIGSERIAL PRIMARY KEY,
  name VARCHAR(255) DEFAULT 'PERSON' NOT NULL
);

create table public.person_role (
    person_id bigint not null,
    role_id bigint not null,
    unique( person_id, role_id ),
    constraint person_fk
        foreign key(person_id) references person(id),
    constraint role_fk
        foreign key(role_id) references role(id)
);


CREATE TABLE public.message (
  id BIGSERIAL PRIMARY KEY,
  message VARCHAR(255) NOT NULL,
  creationdate TIMESTAMP not null,
  chatroom_id bigint not null,
  email VARCHAR(255) NOT NULL,
  constraint message_chatroom_fk
    foreign key(chatroom_id) references chatroom(id)
);


INSERT INTO
    public.person ( username, firstName, lastName, email, subject )
VALUES
    ('Mark', 'Mark','Haberpursch', 'weasel@tutamail.com', 'd9db0996-dc80-4f04-b5b4-71ee3dda8778' )
    ;


--INSERT INTO
--    public.chatroom (name)
--VALUES
--    ('ni | wwweasel')
--    ;

--INSERT INTO
--    public.person_chatroom ( person_id, chatroom_id )
--VALUES
--    ( 1, 1 ),
--    ( 2, 1 )
--    ;

INSERT INTO
    public.role (name)
VALUES
    ('PERSON'),
    ('WWWEASEL')
    ;

INSERT INTO
    public.person_role ( person_id, role_id )
VALUES
    ( 1, 1 ),
    ( 1, 2 )
    ;


--INSERT INTO
--    public.message ( message, chatroom_id, creationdate, email )
--VALUES
--    ( 'Message 1', 1, '2016-06-14 19:19:25', 'ni@ni.com' ),
--	( 'Message 2', 1, '2016-06-14 19:17:25', 'weasel@tutamail.com' ),
--	( 'Message 3', 1, NOW(), 'ni@ni.com' ),
--	( 'Message 4', 1, NOW(), 'weasel@tutamail.com' )
--	;
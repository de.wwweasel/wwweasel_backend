package www.persistenceAdapters;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface RoleJpaRepo extends JpaRepository<RoleJpa, Long> {

    @Query("SELECT r FROM RoleJpa r WHERE r.name = :name")
    Optional<RoleJpa> findByName(String name);

}

package www.persistenceAdapters;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
interface MessageJpaRepo extends JpaRepository<MessageJpa, Long> {

    @Query( value = "Select m.* " +
            "From message as m " +
            "where m.chatroom = :chatroom_id " +
            ";", nativeQuery=true )
    ArrayList<MessageJpa> findByChatroomId(Long chatroom_id);
}

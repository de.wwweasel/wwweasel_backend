package www.persistenceAdapters;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import www.domain.Chatroom;
import www.domain.Message;
import www.domain.Person;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface ChatsByEmailPersistenceMapper {

    @Mapping(source = "persons", target="persons", qualifiedByName = "personsChatroom")
    Chatroom jpaToChatroom(ChatroomJpa jpa);
    // The following stops the inifinite loop when mapping Persons and Chatrooms

    // Generate Persons without Chatrooms and without Roles
    @Named("personsChatroom")
    default List<Person> personsChatroom(List<PersonJpa> persons ){
        return persons.stream()
                .map( personJpa -> new Person(
                        personJpa.getId(),
                        personJpa.getUsername(),
                        personJpa.getFirstName(),
                        personJpa.getLastName(),
                        personJpa.getEmail(),
                        personJpa.getSubject()
                ))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    // Messages
    @Mapping(target="chatroom", ignore = true)
    Message jpaToMessage(MessageJpa jpa);
}

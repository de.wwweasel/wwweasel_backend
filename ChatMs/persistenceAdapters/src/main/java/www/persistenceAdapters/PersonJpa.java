package www.persistenceAdapters;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import java.util.*;


@Entity
@Table(name="person", schema = "public")
class PersonJpa { // 'default' packageScope!

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;
    @Column(name="username")
    private String username;
    @Column(name="firstname")
    private String firstName;
    @Column(name="lastname")
    private String lastName;
    @Column(name="email", unique=true)
    private String email;
    @Column(name="subject")
    private String subject;

    // Eager means when fetching a User from the DB JPA will automatically also fetch all of its Chatrooms!
    // 'cascade = CascadeType.ALL' would automatically persist related table Chatroom as well when persisting User, but
    // because you would get oftentimes a 'duplicate key violation error' on Chatroom you don't want that!
    // Instead the ServiceLayer persists both Chatroom and User manually and checks if Chatroom already exists to avoid 'duplicate key violation error'.
    @ManyToMany( fetch = FetchType.LAZY )//cascade = CascadeType.ALL,
    //@Cascade({ CascadeType.SAVE_UPDATE, CascadeType.MERGE, CascadeType.PERSIST})
    @JoinTable(
            name = "person_chatroom",
            joinColumns = @JoinColumn(name = "person_id"),
            inverseJoinColumns = @JoinColumn(name = "chatroom_id")
    )
    private List<ChatroomJpa> chatrooms = new ArrayList<>();

    /*
        Its LAZY, because you manually fetch all Roles, Chatrooms as well as their Messages
        with an SQL statement in the PersonJpaRepo.
        * */
    @ManyToMany( fetch = FetchType.LAZY)//cascade = CascadeType.ALL,
    @Cascade({ CascadeType.SAVE_UPDATE, CascadeType.MERGE, CascadeType.PERSIST})
    @JoinTable(
            name = "person_role",
            joinColumns = @JoinColumn(name = "person_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    private List<RoleJpa> roles = new ArrayList<>();


    public PersonJpa(){}

    public PersonJpa(Long id, String username, String firstName, String lastName, String email, String subject) {
        this.id = id;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.subject = subject;
    }

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public String getUsername() { return username; }
    public void setUsername(String username) { this.username = username; }

    public String getFirstName() { return firstName; }
    public void setFirstName(String firstName) { this.firstName = firstName; }

    public String getLastName() { return lastName; }
    public void setLastName(String lastName) { this.lastName = lastName; }

    public String getEmail() { return email; }
    public void setEmail(String email) { this.email = email; }

    public String getSubject() { return subject; }
    public void setSubject(String subject) { this.subject = subject; }

    public List<ChatroomJpa> getChatrooms() { return chatrooms; }
    public void setChatrooms(List<ChatroomJpa> chatrooms) { this.chatrooms = chatrooms; }

    public List<RoleJpa> getRoles() { return roles; }
    public void setRoles(List<RoleJpa> roles) { this.roles = roles; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonJpa personJpa = (PersonJpa) o;
        return Objects.equals(email, personJpa.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email);
    }


}

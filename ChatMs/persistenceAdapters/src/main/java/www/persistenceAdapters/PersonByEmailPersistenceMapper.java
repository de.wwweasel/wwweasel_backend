package www.persistenceAdapters;

import org.mapstruct.*;
import www.domain.Chatroom;
import www.domain.Message;
import www.domain.Person;
import www.domain.Role;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface PersonByEmailPersistenceMapper {

    Person jpaToPerson(PersonJpa jpa );
    // This method executes the following

    // Roles
    @Mapping(target="persons", ignore = true)
    Role jpaToRole(RoleJpa jpa);

    // Chatrooms
    @Mapping(source = "persons", target="persons", qualifiedByName = "personsChatroom")
    Chatroom jpaToChatroom(ChatroomJpa jpa);
    // The following stops the inifinite loop when mapping Persons and Chatrooms

    @Named("personsChatroom")
    default List<Person> personsChatroom(List<PersonJpa> persons ){
        return persons.stream()
                .map( personJpa -> new Person(
                        personJpa.getId(),
                        personJpa.getUsername(),
                        personJpa.getFirstName(),
                        personJpa.getLastName(),
                        personJpa.getEmail(),
                        personJpa.getSubject()
                ))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    // Messages
    @Mapping(target="chatroom", ignore = true)
    Message jpaToMessage(MessageJpa jpa);


    ///////////////////////////////////////////////////////////////////

}

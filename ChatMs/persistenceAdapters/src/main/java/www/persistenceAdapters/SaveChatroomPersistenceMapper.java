package www.persistenceAdapters;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import www.domain.Chatroom;
import www.domain.Message;
import www.domain.Person;
import www.domain.Role;


@Mapper(componentModel = "spring")
public abstract class SaveChatroomPersistenceMapper {

    abstract ChatroomJpa chatroomToJpa(Chatroom chatroom);
    // The above calls the following methods

    @Mapping(target="chatrooms", ignore = true)
    abstract PersonJpa personToJpa(Person person);
    @Mapping(target="persons", ignore = true)
    abstract RoleJpa roleToJpa(Role role);
    @Mapping(target="chatroom", ignore = true)
    abstract MessageJpa messageToJpa(Message message);


    ///////////////////////////////////////////////////////////////////


    abstract Chatroom jpaToChatroom(ChatroomJpa jpa);
    // The above calls the following methods

    @Mapping(target="chatrooms", ignore = true)
    abstract Person jpaToPerson(PersonJpa jpa);
    @Mapping(target="persons", ignore = true)
    abstract Role jpaToRole(RoleJpa jpa);
    @Mapping(target="chatroom", ignore = true)
    abstract Message jpaToMessage(MessageJpa jpa);

}

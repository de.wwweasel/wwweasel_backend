package www.persistenceAdapters;

import org.springframework.stereotype.Service;
import www.application.port.out.PersonByEmailPort;
import www.domain.Person;
import javax.transaction.Transactional;
import java.util.Optional;


@Transactional
@Service
public class PersonByEmailPersistenceAdapter implements PersonByEmailPort {

    private final PersonJpaRepo personJpaRepo;
    private final PersonByEmailPersistenceMapper mapper;

    public PersonByEmailPersistenceAdapter(PersonJpaRepo personJpaRepo, PersonByEmailPersistenceMapper mapper) {
        this.personJpaRepo = personJpaRepo;
        this.mapper = mapper;
    }

    @Override
    public Optional<Person> personByEmail(String email) {

        return this.personJpaRepo.findByEmail(email) // This is an Optional of PersonJpa !
                .map( personJpa ->   Optional.of( this.mapper.jpaToPerson( personJpa ) ) )
                .orElseGet( () -> Optional.empty() );

    }
}

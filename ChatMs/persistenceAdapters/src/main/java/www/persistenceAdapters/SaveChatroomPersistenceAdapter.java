package www.persistenceAdapters;

import org.springframework.stereotype.Service;
import www.application.port.out.SaveChatroomPort;
import www.domain.Chatroom;
import javax.transaction.Transactional;


@Transactional
@Service
public class SaveChatroomPersistenceAdapter implements SaveChatroomPort {

    private final ChatroomJpaRepo chatroomJpaRepo;
    private final SaveChatroomPersistenceMapper saveChatroomPersistenceMapper;

    public SaveChatroomPersistenceAdapter(ChatroomJpaRepo chatroomJpaRepo, SaveChatroomPersistenceMapper saveChatroomPersistenceMapper) {
        this.chatroomJpaRepo = chatroomJpaRepo;
        this.saveChatroomPersistenceMapper = saveChatroomPersistenceMapper;
    }


    @Override
    public Chatroom saveChatroom(Chatroom chatroom) {

        ChatroomJpa persistedJpa = this.chatroomJpaRepo.save( this.saveChatroomPersistenceMapper.chatroomToJpa(chatroom) );

        return this.saveChatroomPersistenceMapper.jpaToChatroom( persistedJpa );
    }
}

package www.persistenceAdapters;

import org.springframework.stereotype.Service;
import www.application.port.out.SaveUpdatePersonPort;
import www.domain.Chatroom;
import www.domain.Person;
import www.domain.Role;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Transactional
@Service
public class SaveUpdatePersonPersistenceAdapter implements SaveUpdatePersonPort {


    private final PersonJpaRepo personJpaRepo;
    private final SaveUpdatePersonPersistenceMapper mapper;
    private final RoleJpaRepo roleJpaRepo;
    private final SaveRolePersistenceAdapter saveRolePersistenceAdapter;

    public SaveUpdatePersonPersistenceAdapter(PersonJpaRepo personJpaRepo, SaveUpdatePersonPersistenceMapper mapper, RoleJpaRepo roleJpaRepo, SaveRolePersistenceAdapter saveRolePersistenceAdapter) {
        this.personJpaRepo = personJpaRepo;
        this.mapper = mapper;
        this.roleJpaRepo = roleJpaRepo;
        this.saveRolePersistenceAdapter = saveRolePersistenceAdapter;
    }

    @Override
    public Person saveUpdatePerson(Person person) {
        PersonJpa persistedPersonJpa = this.personJpaRepo.findByEmail(person.getEmail())
                .map(personJpa -> this.updatePerson(personJpa, person))
                .orElseGet(() -> this.persistPerson(person));
        return this.mapper.jpaToPerson( persistedPersonJpa );
    }


    private PersonJpa updatePerson(PersonJpa jpa, Person person){
        System.out.println("Will update person: " + person.getChatrooms());

        jpa.setUsername(person.getUsername());
        jpa.setFirstName(person.getFirstName());
        jpa.setLastName(person.getLastName());
        jpa.setSubject(person.getSubject());
        // Person comes in WITHOUT chats if called by SyncPersonService!
        // Person/Weasel comes in WITH chat when called by CreateDefaultChatService!
        if(person.getChatrooms().size()>0){
            jpa.setChatrooms(this.chatroomsToJpas( person.getChatrooms() ));
        }
        jpa.setRoles( this.get_roles(person.getRoles()) );
        return this.personJpaRepo.save( jpa );
    }


    private PersonJpa persistPerson(Person person){
        System.out.println("Will persist new person: " + person.getEmail() );
        PersonJpa personJpa = this.mapper.personToJpa(person);
        personJpa.setRoles( this.get_roles(person.getRoles()) );
        return this.personJpaRepo.save( personJpa );

    }


    private ArrayList<RoleJpa> get_roles(List<Role> roles){
        ArrayList<RoleJpa> jpaRoles = new ArrayList<>();
        for (int i = 0; i < roles.size(); i++) {
            Optional<RoleJpa> roleJpa = this.roleJpaRepo.findByName(roles.get(i).getName());
            if(roleJpa.isPresent()){
                jpaRoles.add(roleJpa.get());
            }else{
                RoleJpa jpa = new RoleJpa();
                jpa.setName(roles.get(i).getName());
                jpa.setPersons(new ArrayList<>());
                jpaRoles.add(jpa);
            }
        }
        return jpaRoles;
    }

    private List<ChatroomJpa> chatroomsToJpas(List<Chatroom> chatrooms){
        return chatrooms.stream()
                .map(chatroom -> this.mapper.chatroomToJpa(chatroom))
                .collect(Collectors.toCollection(ArrayList::new));
    }
}

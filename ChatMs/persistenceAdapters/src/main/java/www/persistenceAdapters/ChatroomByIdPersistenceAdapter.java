package www.persistenceAdapters;

import org.springframework.stereotype.Service;
import www.application.port.out.ChatroomByIdPort;
import www.domain.Chatroom;
import javax.transaction.Transactional;
import java.util.Optional;


@Transactional
@Service
public class ChatroomByIdPersistenceAdapter implements ChatroomByIdPort {

    private final ChatroomJpaRepo chatroomJpaRepo;
    private final ChatroomByIdPersistenceMapper mapper;

    public ChatroomByIdPersistenceAdapter(ChatroomJpaRepo chatroomJpaRepo, ChatroomByIdPersistenceMapper mapper) {
        this.chatroomJpaRepo = chatroomJpaRepo;
        this.mapper = mapper;
    }

    @Override
    public Optional<Chatroom> chatroomById(Long id) {

        return this.chatroomJpaRepo.findById(id)
            .map( chatroomJpa -> Optional.of( this.mapper.jpaToChatroom( chatroomJpa ) ) )
            .orElseGet( () -> Optional.empty() );
    }
}

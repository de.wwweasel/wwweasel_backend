package www.persistenceAdapters;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.TreeSet;

@Repository
interface ChatroomJpaRepo extends JpaRepository<ChatroomJpa, Long> {

    /*
        The following somehow will loose the ordering of the messages
     */
//    @Query( value = "Select c.* " +
//            "From chatroom as c " +
//            "left join person_chatroom as pc on pc.chatroom_id = c.id " +
//            "left join person as p on p.id = pc.person_id " +
//            "left join message m on m.chatroom_id = c.id " +
//            "where p.email = :userEmail " +
//            "order by m.creationdate  " +
//            ";", nativeQuery=true )
//    ArrayList<ChatroomJpa> findByEmail(String userEmail);



    /*  The following will also join the Messages that will be sorted by creationdate
        because in the ChatroomJpa @Entity the messages field is annotated with @OrderBy("creationdate ASC")
     */
    @Query("SELECT c FROM ChatroomJpa c JOIN FETCH c.persons p WHERE p.email = :email")
    HashSet<ChatroomJpa> findByEmail(String email);

}

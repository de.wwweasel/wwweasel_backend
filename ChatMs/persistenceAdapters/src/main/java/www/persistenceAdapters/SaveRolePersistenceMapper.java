package www.persistenceAdapters;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import www.domain.Person;
import www.domain.Role;

@Mapper(componentModel = "spring")
public interface SaveRolePersistenceMapper {

    @Mapping(target="persons", ignore = true)
    RoleJpa roleToJpa(Role role);

//    @Mapping(target="roles", ignore = true)
//    PersonJpa personToJpa(Person person);

    //////////////////////////////////////

    @Mapping(target="persons", ignore = true)
    Role jpaToRole(RoleJpa roleJpa);

//    @Mapping(target="roles", ignore = true)
//    Person jpaToPerson(PersonJpa personJpa);


}

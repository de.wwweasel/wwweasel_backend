package www.persistenceAdapters;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name="role", schema = "public")
public class RoleJpa {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @Column(name="name")
    private String name;

    @JsonIgnore
    @ManyToMany(mappedBy = "roles", fetch = FetchType.LAZY)// fetch=FetchType.EAGER, cascade=CascadeType.ALL
    //@Cascade({ CascadeType.SAVE_UPDATE, CascadeType.MERGE, CascadeType.PERSIST})
    private List<PersonJpa> persons = new ArrayList<>();


    public RoleJpa(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<PersonJpa> getPersons() { return persons; }
    public void setPersons(List<PersonJpa> persons) { this.persons = persons; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoleJpa roleJpa = (RoleJpa) o;
        return Objects.equals(id, roleJpa.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

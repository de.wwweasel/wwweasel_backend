package www.persistenceAdapters;

import org.mapstruct.*;
import www.domain.Chatroom;
import www.domain.Message;
import www.domain.Person;
import www.domain.Role;

import java.util.*;
import java.util.stream.Collectors;


@Mapper(componentModel = "spring")
public abstract class ChatroomByIdPersistenceMapper {

    abstract Chatroom jpaToChatroom(ChatroomJpa jpa);
    // The above calls the following methods

    @Mapping(target="chatrooms", ignore = true)
    abstract Person jpaToPerson(PersonJpa jpa);
    @Mapping(target="persons", ignore = true)
    abstract Role jpaToRole(RoleJpa jpa);
    @Mapping(target="chatroom", ignore = true)
    abstract Message jpaToMessage(MessageJpa jpa);

    //////////////////////////////////////////////////

}

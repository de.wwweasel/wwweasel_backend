package www.persistenceAdapters;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
interface PersonJpaRepo extends JpaRepository<PersonJpa, Long> {

    // This Query loads all Persons with all Chatrooms and all Messages !
    @Query( value = "Select p.* " +
            "From person as p " +
            "left join person_role as pr on pr.person_id = p.id " +
            "left join role as r on r.id = pr.role_id " +
            "left join person_chatroom as pc on pc.person_id = p.id " +
            "left join chatroom as c on c.id = pc.chatroom_id " +
            "left join message as m on m.chatroom_id = c.id " +
            "where p.email = :email " +
            ";", nativeQuery=true )
    Optional<PersonJpa> findByEmail(String email);


//    @Query("SELECT p FROM PersonJpa p LEFT JOIN FETCH p.roles LEFT JOIN FETCH chatrooms LEFT JOIN FETCH messages WHERE p.email = :email ")
//    Optional<PersonJpa> findByEmail(String email);

}

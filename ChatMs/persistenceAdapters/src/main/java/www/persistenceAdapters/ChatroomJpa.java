package www.persistenceAdapters;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.SortNatural;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.*;

@Table(name = "chatroom", schema = "public")
@Entity
class ChatroomJpa {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;
    @Column(name="name")
    private String name;

    @JsonIgnore
    @ManyToMany(mappedBy = "chatrooms", fetch = FetchType.LAZY)// fetch=FetchType.EAGER, cascade=CascadeType.ALL
    //@Cascade({ CascadeType.SAVE_UPDATE, CascadeType.MERGE, CascadeType.PERSIST})
    private List<PersonJpa> persons = new ArrayList<>();
    //private Comparator<LocalDateTime> comparator = Comparator.comparing(MessageJpa::getCreationdate);
    @JsonIgnore
    @OneToMany(mappedBy= "chatroom", fetch = FetchType.LAZY)
    @OrderBy("creationdate ASC")
    private List<MessageJpa> messages = new ArrayList<>();


    public ChatroomJpa(){}
    public ChatroomJpa(String name){
        this.name = name;
    }


    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public List<PersonJpa> getPersons() { return persons; }
    public void setPersons(List<PersonJpa> persons) { this.persons = persons; }

    public List<MessageJpa> getMessages() { return messages; }
    public void setMessages(List<MessageJpa> messages) { this.messages = messages; }

    @Override
    public String toString() {
        return "Chatroom{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", users=" + persons +
                ", messages=" + messages +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChatroomJpa that = (ChatroomJpa) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

package www.persistenceAdapters;

import org.springframework.stereotype.Service;
import www.application.port.out.SaveRolePort;
import www.domain.Role;

import javax.transaction.Transactional;

@Transactional
@Service
public class SaveRolePersistenceAdapter implements SaveRolePort {

    private final RoleJpaRepo roleJpaRepo;
    private final SaveRolePersistenceMapper saveRolePersistenceMapper;

    public SaveRolePersistenceAdapter(RoleJpaRepo roleJpaRepo, SaveRolePersistenceMapper saveRolePersistenceMapper) {
        this.roleJpaRepo = roleJpaRepo;
        this.saveRolePersistenceMapper = saveRolePersistenceMapper;
    }

    @Override
    public Role saveRole(Role role) {
        /*
            Check if Role exists in the DB before persisting it, because it should be Unique.
         */
        return this.saveRolePersistenceMapper.jpaToRole(
                this.roleJpaRepo.findByName(role.getName())
                    .orElseGet(() -> dreck(role))
        );

    }

    public RoleJpa dreck(Role role){
        RoleJpa save = this.roleJpaRepo.save(this.saveRolePersistenceMapper.roleToJpa(role));
        System.out.println("Saved Role with id: " + save.getId() );
        return save;
    }
}

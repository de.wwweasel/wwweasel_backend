package www.persistenceAdapters;

import org.springframework.stereotype.Service;
import www.application.port.out.SaveMessagePort;
import www.domain.Message;

import javax.transaction.Transactional;

@Transactional
@Service
public class SaveMessagePersistenceAdapter implements SaveMessagePort {

    private final MessageJpaRepo messageJpaRepo;
    private final SaveMessagePersistenceMapper mapper;

    public SaveMessagePersistenceAdapter(MessageJpaRepo messageJpaRepo, SaveMessagePersistenceMapper mapper) {
        this.messageJpaRepo = messageJpaRepo;
        this.mapper = mapper;
    }

    @Override
    public Message saveMessage(Message message) {
        return mapper.jpaToMessage( messageJpaRepo.save( mapper.messageToJpa(message) ) );
    }
}

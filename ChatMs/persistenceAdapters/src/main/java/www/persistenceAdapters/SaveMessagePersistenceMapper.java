package www.persistenceAdapters;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import www.domain.Chatroom;
import www.domain.Message;
import www.domain.Person;

import java.util.Set;


@Mapper(componentModel = "spring")
public abstract class SaveMessagePersistenceMapper {

    abstract Message jpaToMessage( MessageJpa jpa);


    ///////////////////////////////////////////////////////////////////


    abstract MessageJpa messageToJpa(Message message);

}

package www.persistenceAdapters;

import org.mapstruct.*;
import www.domain.Chatroom;
import www.domain.Message;
import www.domain.Person;
import www.domain.Role;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@Mapper(componentModel = "spring")
public interface SaveUpdatePersonPersistenceMapper {


    PersonJpa personToJpa(Person person);
    // This method calls the following methods

    // Roles (without infinite loop)
    @Mapping(target="persons", ignore = true)
    RoleJpa roleToJpa(Role role);

    // Chatrooms (without infinite loop)
    @Mapping(source = "persons", target="persons", qualifiedByName = "personsChatroomJpa")
    ChatroomJpa chatroomToJpa(Chatroom chatroom);

    @Named("personsChatroomJpa")
    default List<PersonJpa> personsChatroomJpa(List<Person> persons ){
        // Ignores Chatrooms and Roles
        return persons.stream()
                .map( person -> new PersonJpa(
                        person.getId(),
                        person.getUsername(),
                        person.getFirstName(),
                        person.getLastName(),
                        person.getEmail(),
                        person.getSubject()
                        ))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    // Messages (without infinite loop)
    @Mapping(target="chatroom", ignore = true)
    MessageJpa jpaToMessage(Message message);


    ///////////////////////////////////////////////////////////////////


    Person jpaToPerson( PersonJpa jpa );
    // This method calls the following methods

    // Roles
    @Mapping(target="persons", ignore = true)
    Role jpaToRole(RoleJpa jpa);

    // Chatrooms
    @Mapping(source = "persons", target="persons", qualifiedByName = "personsChatroom")
    Chatroom jpaToChatroom(ChatroomJpa jpa);

    @Named("personsChatroom")
    default List<Person> personsChatroom(List<PersonJpa> persons ){
        return persons.stream()
                .map( personJpa -> new Person(
                    personJpa.getId(),
                        personJpa.getUsername(),
                        personJpa.getFirstName(),
                        personJpa.getLastName(),
                        personJpa.getEmail(),
                        personJpa.getSubject()
                ))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    // Messages
    @Mapping(target="chatroom", ignore = true)
    Message jpaToMessage(MessageJpa jpa);

}

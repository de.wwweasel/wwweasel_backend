package www.persistenceAdapters;

import org.springframework.stereotype.Service;
import www.application.port.out.ChatsByEmailPort;
import www.domain.Chatroom;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.stream.Collectors;

@Transactional
@Service
public class ChatsByEmailPersistenceAdapter implements ChatsByEmailPort {

    private final ChatroomJpaRepo chatroomJpaRepo;
    private final ChatsByEmailPersistenceMapper chatsByEmailPersistenceMapper;

    public ChatsByEmailPersistenceAdapter(ChatroomJpaRepo chatroomJpaRepo, ChatsByEmailPersistenceMapper chatsByEmailPersistenceMapper) {
        this.chatroomJpaRepo = chatroomJpaRepo;
        this.chatsByEmailPersistenceMapper = chatsByEmailPersistenceMapper;
    }


    @Override
    public HashSet<Chatroom> chatsByEmail(String email) {
        return this.chatroomJpaRepo.findByEmail( email )
                .stream()
                .map( chatroomJpa -> this.chatsByEmailPersistenceMapper.jpaToChatroom( chatroomJpa ) )
                .collect(Collectors.toCollection(HashSet::new));
    }
}

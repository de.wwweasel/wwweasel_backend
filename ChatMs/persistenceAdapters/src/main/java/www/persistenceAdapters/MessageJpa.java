package www.persistenceAdapters;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name="message", schema = "public")
class MessageJpa{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String message;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime creationdate;
    @ManyToOne
    private ChatroomJpa chatroom;
    private String email;

    public MessageJpa() {}
    public MessageJpa( String message, ChatroomJpa chatroom, String email){
        this.message = message;
        this.chatroom = chatroom;
        this.email = email;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public LocalDateTime getCreationdate() {
        return creationdate;
    }
    public void setCreationdate(LocalDateTime creationdate) {
        this.creationdate = creationdate;
    }
    public ChatroomJpa getChatroom() {
        return chatroom;
    }
    public void setChatroom(ChatroomJpa chatroom) {
        this.chatroom = chatroom;
    }
    public String getEmail() { return email; }
    public void setEmail(String email) { this.email = email; }

    @PrePersist
    public void prePersist(){
        this.creationdate = LocalDateTime.now();
    }

    @Override
    public String toString() {
        return "MessageJpa{" +
                "id=" + id +
                ", message='" + message + '\'' +
                ", creationdate=" + creationdate +
                ", chatroom=" + chatroom +
                ", useremail='" + email + '\'' +
                '}';
    }

    // COMPARE them by LocalDateTime
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessageJpa that = (MessageJpa) o;
        return Objects.equals(creationdate, that.creationdate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(creationdate);
    }
}

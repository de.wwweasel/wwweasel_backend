package www.persistenceAdapters;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

@SpringBootConfiguration
@EnableAutoConfiguration
public class PersistenceAdaptersTestConfig extends PersistenceAdaptersConfig{
}

package www.persistenceAdapters;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.HashSet;

@DataJpaTest(properties = {"spring.cloud.config.enabled=false"}) // Disable ConfigServer
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Testcontainers
@Sql("/data.sql")
class ChatsByEmailPersistenceAdapterTest {

//    @Container
//    private static PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer("postgres:13-alpine");
//
//    @DynamicPropertySource
//    public static void overrideProperties(DynamicPropertyRegistry registry){
//        registry.add("spring.datasource.url", postgreSQLContainer::getJdbcUrl);
//        registry.add("spring.datasource.username", postgreSQLContainer::getUsername);
//        registry.add("spring.datasource.password", postgreSQLContainer::getPassword);
//    }
//
////    @Test
////    void contextLoads(){
////        System.out.println("Context loads!");
////    }
//
//    @Autowired
//    private ChatsByEmailPersistenceAdapter chatsByEmailPersistenceAdapter;
//    @Autowired
//    private ChatroomJpaRepo chatroomJpaRepo;
//    private ChatsByEmailPersistenceMapper chatsByEmailPersistenceMapper;
//
//
//    // Works but not when building the Container
//    @Test
//    void chatsByEmail() {
//        HashSet<ChatroomJpa> chatrooms = chatroomJpaRepo.findByEmail("ni@ni.com");
//        Assertions.assertNotNull(chatrooms);
//        Assertions.assertEquals(chatrooms.size(), 1);
//
//    }
//
//    @Test
//    void chatsByEmailEmpty() {
//        HashSet<ChatroomJpa> chatrooms = chatroomJpaRepo.findByEmail("fake@fake.com");
//        Assertions.assertNotNull(chatrooms);
//        Assertions.assertEquals(chatrooms.size(), 0);
//
//    }
}
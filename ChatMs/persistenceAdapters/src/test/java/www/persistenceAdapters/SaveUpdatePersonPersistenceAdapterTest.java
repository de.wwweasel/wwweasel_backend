package www.persistenceAdapters;

import org.assertj.core.api.AbstractAtomicReferenceAssert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import www.domain.Person;
import www.domain.Role;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest(properties = {"spring.cloud.config.enabled=false"}) // Disable ConfigServer
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Testcontainers
@Sql("/data.sql")
class SaveUpdatePersonPersistenceAdapterTest {

//    @Container
//    private static PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer("postgres:13-alpine");
//
//    @DynamicPropertySource
//    public static void overrideProperties(DynamicPropertyRegistry registry){
//        registry.add("spring.datasource.url", postgreSQLContainer::getJdbcUrl);
//        registry.add("spring.datasource.username", postgreSQLContainer::getUsername);
//        registry.add("spring.datasource.password", postgreSQLContainer::getPassword);
//    }
//
////    @Test
////    void contextLoads(){
////        System.out.println("Context loads!");
////    }
//
//    @Autowired
//    SaveUpdatePersonPersistenceAdapter saveUpdatePersonPersistenceAdapter;
//    @Autowired
//    SaveRolePersistenceAdapter saveRolePersistenceAdapter;
//
//
//    @Test
//    void saveUpdatePerson_save() {
//
//        Person unknown = new Person();
//        unknown.setUsername("unknown");
//        unknown.setFirstName("firstname");
//        unknown.setLastName("lastname");
//        unknown.setEmail("unknown@unknow.com");
//        unknown.setSubject("Loado123123");
//
//        Person result_person = saveUpdatePersonPersistenceAdapter.saveUpdatePerson( unknown );
//
//        Assertions.assertNotNull( result_person );
//        Assertions.assertEquals( result_person.getEmail(), result_person.getEmail());
//        Assertions.assertEquals( result_person.getUsername(), result_person.getUsername());
//        Assertions.assertEquals( result_person.getFirstName(), result_person.getFirstName());
//        Assertions.assertEquals( result_person.getLastName(), result_person.getLastName());
//
//    }
//
//
//    @Test
//    void saveUpdatePerson_update() {
//
//        Person unknown = new Person();
//        unknown.setUsername("unknown");
//        unknown.setFirstName("firstname");
//        unknown.setLastName("lastname");
//        unknown.setEmail("unknown@unknow.com");
//        unknown.setSubject("Loado123123");
//        Role person_role = new Role("PERSON");
//        ArrayList<Role> roles = new ArrayList<>();
//        roles.add(person_role);
//
//
//        Role other_role = new Role("LAN");
//        roles.add(other_role);
//        unknown.setRoles(roles);
//
//        Person result_person = saveUpdatePersonPersistenceAdapter.saveUpdatePerson( unknown );
//
//        Assertions.assertNotNull( result_person );
//        Assertions.assertEquals( result_person.getEmail(), unknown.getEmail());
//        Assertions.assertEquals( result_person.getUsername(), unknown.getUsername());
//        Assertions.assertEquals( result_person.getFirstName(), unknown.getFirstName());
//        Assertions.assertEquals( result_person.getLastName(), unknown.getLastName());
//        System.out.println("ID: -> " + result_person.getRoles().get(0).getId());
//        System.out.println("ID: -> " + result_person.getRoles().get(1).getId());
//        //Assertions.assertEquals( result_person.getRoles().get(0).getId(), 1L);
////        Assertions.assertEquals( result_person.getRoles().size(), 2);
////        System.out.println("ID: -> " + result_person.getRoles().get(1).getId());
////        Assertions.assertEquals( result_person.getRoles().get(1).getId(), 3);
//
//        //
//        unknown.setUsername("other_username");
//        unknown.setFirstName("other_firstname");
//        unknown.setLastName("other_lastname");
//        Role wwweaselrole = new Role("WWWEASEL");
//        roles.add(wwweaselrole);
//        unknown.setRoles(roles);
//
//        Person other_person = saveUpdatePersonPersistenceAdapter.saveUpdatePerson( unknown );
//
//        Assertions.assertNotNull( other_person );
//        Assertions.assertEquals( other_person.getEmail(), unknown.getEmail());
//        Assertions.assertEquals( other_person.getUsername(), unknown.getUsername());
//        Assertions.assertEquals( other_person.getFirstName(), unknown.getFirstName());
//        Assertions.assertEquals( other_person.getLastName(), unknown.getLastName());
//        Assertions.assertEquals( other_person.getRoles().size(), 3 );
//    }

}
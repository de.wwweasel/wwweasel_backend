package www.persistenceAdapters;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import www.domain.Chatroom;
import www.domain.Message;


@DataJpaTest(properties = {"spring.cloud.config.enabled=false"}) // Disable ConfigServer
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Testcontainers
@Sql("/data.sql")
class SaveMessagePersistenceAdapterTest {

//    @Container
//    private static PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer("postgres:13-alpine");
//
//    @DynamicPropertySource
//    public static void overrideProperties(DynamicPropertyRegistry registry){
//        registry.add("spring.datasource.url", postgreSQLContainer::getJdbcUrl);
//        registry.add("spring.datasource.username", postgreSQLContainer::getUsername);
//        registry.add("spring.datasource.password", postgreSQLContainer::getPassword);
//    }
//
//    @Autowired
//    SaveMessagePersistenceAdapter saveMessagePersistenceAdapter;
//
//    // Works but not when building the Container
//
////    @Test
////    void contextLoads(){
////        System.out.println("Context loads!");
////    }
//
//
//    @Test
//    void saveMessage() {
//
//        Message message = new Message();
//        message.setMessage("TestMessageContent");
//        Chatroom chatroom = new Chatroom("ni | wwweasel");// Already exists in DB
//        chatroom.setId(1L);
//        message.setChatroom(chatroom);
//        message.setEmail("ni@ni.com");
//
//        Message persistedMessage = this.saveMessagePersistenceAdapter.saveMessage(message);
//
//        Assertions.assertEquals(persistedMessage.getMessage(),message.getMessage());
//        Assertions.assertNotNull(persistedMessage.getId());
//        Assertions.assertNotNull(persistedMessage.getCreationdate());
//        Assertions.assertEquals(persistedMessage.getChatroom().getName(), chatroom.getName());
//    }
}